Tokera Kernel Module
========================================

This kernel module emulates a virtual file system in kernel space and provides a high performance pipelining of IO requests through layer II networking protocols
to distributed disks on remote nodes. It also provides the hook into the kernel file system for calls to the Tokera API that emulate a real file system

Some of the features of this are described below
------------------------------------------------
* fully non-blocking architecture
* zero copy architecture
* configuration free design
* bypasses IO redundant scheduling mechanisms
* automatically resends IO commands that timeout
* communicates using the TokNet ethernet packets
* supports RAID 0,1 and 10
* automatically checks and replicates to mirror disks
* built in throttling mechanisms
* virtual file system to emulate files via REST services

Remaining Tasks
---------------
* ~~fix issue where the file system does not persist between restarts~~
* ~~enable distributed reads when the volume is in a synchronized state~~
* ~~solve issue with poor performance when replicating large amounts of data~~
* ~~fix the file remove operation (which does not work properly anymore)~~
* ~~re-enable the sync writes when the volume has been synchronized~~
* integrate with the Extent Mapping logic built into the Tokera API
* TRIM the disk when allocating and destroying extents

Remaining Features
------------------
* implement the cache coherency protocol
* implement memory based backing device
* implement fdisk operation for recovering lost extents
* test the mirror recovery code for failed disks

Building and Compiling
======================

Dependencies
------------
* recommend debian wheezy as the distribution
* must have built and installed TokKernel on distribution
* must be running on the TokKernel
* TokKernel headers are required to build this driver
* download and install netbeans 8.0
* `apt-get install make`

Commands
--------
* `make all`  
    downloads all the dependencies and builds the kernel
* `make build`  
    only build the kernel by itself  
    note: must be executed after a normal `make all`
* `make install`  
    installs the TokDisk kernel module into the running distribution  
    note: the existing TokDisk kernel module must be unloaded before installing another one
* `unmount /tokera`  
    unloads the VFS mounting point that will prevent the TokDisk kernel module from unloading  
    note: you must have no more bash processes in the tokera directory for this command to execute successfully 
* `rmmod tokdisk`  
    unloads the TokDisk kernel module from memory  
    note: the existing VFS mounting point must first be unmounted before attempting to unload the module

Configuration
=============
* Enable DEBUG mode by editing [tokdisk.c] and uncommenting the following line  
    `#define TOK_DEBUG`
* Force all the virtual storages to be rebuilt (if they become corrupted) by editing [tokdefine.h] and uncommenting the following line  
    `#define TOK_FORCE_REBUILD`
* During performance issues the following steps can be followed to trace down where the bottleneck resides
    1. Enable DEBUG mode (see above)
    2. Enable PROFILING by editing the [tokdefine.h] and uncommenting the following line(s)  
        `#define TOK_PROFILE`  
        `#define TOK_ENABLE_RTT_SAMPLING`
    3. Build the tokdisk (e.g. make build) and reload the kernel module (e.g. `umount /tokera`, `rmmod tokdisk`, `make install`)
    4. Set the start point of the profiling by reading the profile VFS file in any of the meta folders (e.g. `cat /tokera/var/a04*/.meta/profile`)
    5. Run load tests
        * Example write load test (`dd if=/dev/zero of=test bs=4096 count=30000`)
        * Example read load test (`dd if=test of=/dev/null bs=4096`)
    6. While the load test is executing check the RTT times of IO by executing the following command (`cat /tokera/var/a04*/.meta/rtt`)
    7. Check where the time is being load in the RTT profile, this should show if its network, disk or CPU
    8. Investigate any bottlenecks on particular disks or on particular sub-systems
    9. Once the load test is complete then read the profile report again (e.g. `cat /tokera/var/a04*/.meta/profile`)
    10. Copy the results into a suitable tool and order the CSV functions by elapsed time
    11. Ignore any functions that are only called a few times (i.e. < 100 times)
    12. Investigate any functions that appear abnormal

Cache Coherency Protocol
========================

|   State      | Clean/Dirty |  Unique  |  Can Write  | Can Forward |  Forced To |  Silent To  |
| ------------ | ----------- | -------- | ----------- | ----------- | ---------- | ----------- |
| (M)odified   | Dirty       | Yes      | Yes         | Yes         | OI         | EI          |
| (E)xclusive  | Clean       | Yes      | Yes         | Yes         | FS         | MI          |
| (O)wned      | Dirty       | Yes      | Yes         | Yes         | MSI        | F           |
| (F)orwarding | Clean       | Yes      | Yes         | Yes         | EI         |             |
| (S)hared     | Either      | No       | No          | No          | MEI        |             |
| (I)nvalid    | N/A         | N/A      | N/A         | N/A         | ME         |             |

Modified
--------
* Page is dirty and is only referenced in this node, it must be written back asynchronously while in this state
* If the page is flushed then it is synchronously written to backing device
* Once written to backing memory the state is silently changed to 'Exclusive' and it becomes possible for it to be evicted from memory when memory pressure increases
* The page can also be forced out which will cause a sychronous write and move the page to the 'Invalid' state
* Other nodes are not allowed to read or write to this page without first forcing this page into another state
* Other nodes are allowed to force this node to the 'Owned' state in order to get a read-only copy of the page and eventually take ownership of it
* Other nodes are also allowed to force this node to the 'Invalid' state and take ownership ('Exclusive') of the page however this must be done in a distributed transaction
  and the data must be first written to the backing device

Exclusive
---------
* Page is clean and is only referenced in this node, it is safe to evict this page from memory (move to 'Invalid' state) silently as it is already in the backing device
* If the page is written to then it will silently move to the 'Modified' state at which point it will attempt to asynchronously writeback to the backing device
* Other nodes are not allowed to read or write to this page without first forcing this page into another state
* Other nodes are allowed to force this node to the 'Forwarding' state in order to get a read-only copy of the page and eventually take ownership of it
* Other nodes are also allowed to force this node to the 'Shared' state and take ownership (thus moving themselves into the 'Forwarding') of the page however this
  must be done in a distributed transaction

Owned
-----
* Page is dirty and other nodes have a read-only copy of the page, it must be written back asynchronously while in this state (until then it can't be evicted)
* Once the page is successfully written to the backing device it will silently move to the 'Forwarding' state
* All writes are blocked on this page until its state transitions to another state
* While in this state any writes will block until the state is forced to 'Modified' state which will require that the page is evicted
  from any other node that has a 'Shared' copy of it using distributed transactions (which is a relatively expensive operation)
* Other nodes are allowed to read the page (and create more copies of it with distributed transaction) but can not perform any writes
* Other nodes are also allowed to force this node to the 'Invalid' state and take ownership (thus moving themselves into the 'Forwarding' state however this must
  be done in a distributed transaction and the data must be first written to the backing device
* Other nodes must inform the node that holds 'Owned' state when they evict a 'Shared' copy of the page, once the number of copies reaches a single copy then
  the page is moved to the 'Modified' state

Forwarding
----------
* Page is clean and other nodes have a read-only copy of the page
* The page can be evicted from memory but only after it's 'Forwarding' state is transferred to another node using a distributed transaction hence it
  should only be executed as a last resort
* Upon moving the page to a new owner if the page is the only copy then the new owner will automatically get the 'Exclusive' state thus removing any need to
  coordinate with other nodes (hence significantly reducing the latency of future IO)
* While in this state any writes will block until the state is forced to the 'Exclusive' state which will require that the page is evicted
  from any other node that has a 'Shared' copy of it using a distributed transaction
* Other nodes are allowed to read the page (and create more copies of it with a distributed transaction) but can not perform any writes
* Other nodes are also allowed to force this node to the 'Invalid' state and take ownership (thus moving themselves into the 'Exclusive' state however this must
  be done in a distributed transaction
* Other nodes must inform the node that holds 'Forwarding' state when they evict a 'Shared' copy of the page, once the number of copies reaches a single copy
  then the page is moved to the 'Exclusive' state

Shared
------
* Page could be dirty or clean in this state thus the page simple represents a read-only copy of a page owned by another machine
* Reads from the page are perfectly fine and do not block however writes will cause blocking as ownership of the page is transferred
* Ownership of the page is transferred by contacting the owning node using a distributing transaction and evicting the page, once the page
  is evicted from the previous owner the new node now owns it with an 'Exclusive' state
* Shared pages can be evicted from memory but the owning node must be informed whenever the page is evicted using a distributed transaction

Invalid
-------
* The page does not exist anywhere and thus anyone can 