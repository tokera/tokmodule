int                 tok_chr_net_open(struct inode *inode, struct file * file);
int                 tok_chr_net_close(struct inode *inode, struct file *file);
long                tok_chr_net_ioctl(struct file *file, unsigned int cmd, unsigned long arg);