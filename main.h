#ifndef TOKMAIN_H
#define	TOKMAIN_H

/* Kernel Programming */
#ifndef MODULE
#define MODULE
#endif
#ifndef LINUX
#define LINUX
#endif
#ifndef __KERNEL__
#define __KERNEL__
#endif

#ifndef CONFIG_BLOCK
#define CONFIG_BLOCK
#endif

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/net.h>
#include <linux/fs.h>
#include <linux/signal.h>
#include <linux/sched/signal.h>
#include <linux/file.h>
#include <linux/splice.h>
#include <linux/miscdevice.h>
#include <linux/poll.h>
#include <linux/elf.h>
#include <linux/hdreg.h>
#include <linux/skbuff.h>
#include <linux/slab.h>
#include <linux/major.h>
#include <linux/errno.h>
#include <linux/fcntl.h>
#include <linux/if_ether.h>
#include <net/ipv6.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <linux/tcp.h>
#include <linux/crc32.h>
#include <linux/rbtree.h>
#include <linux/crypto.h>
#include <linux/list.h>
#include <linux/if_ether.h>
#include <linux/if_vlan.h>
#include <crypto/twofish.h>
#include <crypto/aes.h>
#include <crypto/ctr.h>
#include <crypto/algapi.h>
#include <linux/crypto.h>
#include <linux/scatterlist.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/smp.h>
#include <linux/random.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/unistd.h>
#include <linux/parser.h>
#include <linux/dcache.h>
#include <linux/aio.h>
#include <linux/blkdev.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/list.h>
#include <linux/namei.h>
#include <linux/time.h>
#include <linux/rcupdate.h>
#include <linux/workqueue.h>
#include <linux/delay.h>
#include <linux/wait.h>
#include <linux/netdevice.h>
#include <linux/rtnetlink.h>
#include <net/rtnetlink.h>
#include <linux/ipv6.h>
#include <uapi/linux/if.h>
#include <uapi/linux/if_arp.h>
#include <linux/etherdevice.h>
#include <linux/u64_stats_sync.h>
#include <linux/sockios.h>
#include <linux/stacktrace.h>
#include <linux/vmalloc.h>
#include <linux/preempt.h>
//#include <asm/system.h>
#include <asm/uaccess.h>
#include <linux/if_packet.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/if_ether.h>
#include <linux/bio.h>
#include <linux/genhd.h>
#include <linux/slab.h>
#include <linux/crypto.h>
#include <linux/rbtree.h>
#include <linux/splice.h>
#include <linux/proc_fs.h>
#include <linux/version.h>
#include <linux/semaphore.h>
#include <linux/uidgid.h>
#include <linux/netdevice.h>
#include <linux/scatterlist.h>
#include <linux/ata.h>

#include "defines.h"
#include "types.h"
#include "helper.h"
#include "net.h"
#include "chr.h"

extern struct tokroot                           root;
extern struct file_operations                   tok_chr_net_fops;
extern struct miscdevice                        tok_chr_net_dev;
extern struct rtnl_link_ops                     tok_net_link_ops __read_mostly;

extern struct packet_type                       tok_pt_net;
extern struct packet_type                       tok_pt_ip6;

int tok_process(struct sk_buff *rcv, int steered);

#endif
