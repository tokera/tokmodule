inline void*                        tok_malloc(size_t size);
inline void                         tok_free(const void* ptr);
void                                tok_d_add(struct dentry* entry, struct inode* inode);
tok_mac_t                           tok_generate_mac(void);
inline uint64_t                     tok_get_random_uint64(void);
inline int                          tok_down_try(struct semaphore* sem);

inline tok_time_t                   tok_time_get(void);
inline tok_time_t                   tok_time_atomic_read(tok_time_t* ptime);
inline s64                          tok_time_to_ms(tok_time_t time);
inline tok_time_t                   tok_time_add_ms(tok_time_t time, s64 ms);
inline unsigned long                tok_time_to_jiffies(tok_time_t time);

inline void                         tok_wake_up_process(struct task_struct* thread);
uint64_t                            tok_crc64(uint64_t seed, const void *_data, size_t len);

inline ino_t                        tok_id_to_ino(tok_id_t id);

inline __pure struct page*          pgv_to_page(void *addr);
#ifdef TOK_DEBUG_DATA
void                                tok_debug_bytes(const char* msg, void *_data, ssize_t data_size);
#endif
#ifdef TOK_DEBUG_DATA
void                                tok_debug_bytes(const char* msg, void *_data, ssize_t data_size);
#endif

void                                basic_checks(int panic);

void                                tok_buf_init(tok_buf_t* buf);
int                                 tok_buf_pad(tok_buf_t* buf, int offset, int amount);
int                                 tok_buf_append(tok_buf_t* buf, void* data, int amount);
int                                 tok_buf_write(tok_buf_t* buf, void* data, int offset, int amount);
int                                 tok_buf_vprintf(tok_buf_t* buf, const char *fmt, va_list ap);
int                                 tok_buf_printf(tok_buf_t* buf, const char *fmt, ...);
void                                tok_buf_release(tok_buf_t* buf);
int                                 tok_buf_calc_max(int offset, int amount);
