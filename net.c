#include "main.h"

void tok_net_skb_headers_offset_update(struct sk_buff *skb, int off) {
    /* Only adjust this if it actually is csum_start rather than csum */
    if (skb->ip_summed == CHECKSUM_PARTIAL)
        skb->csum_start += off;

    /* {transport,network,mac}_header and tail are relative to skb->head */
    skb->transport_header += off;
    skb->network_header += off;

    if (skb_mac_header_was_set(skb))
        skb->mac_header += off;

    skb->inner_transport_header += off;
    skb->inner_network_header += off;
    skb->inner_mac_header += off;
}

int tok_net_init(struct net_device *dev) {

    dev->dstats = alloc_percpu(struct pcpu_dstats);
    if (!dev->dstats) {
        return -ENOMEM;
    }
    
    return 0;
};

void tok_net_uninit(struct net_device *dev) {
    
    free_percpu(dev->dstats);
};

int tok_net_change_carrier(struct net_device *dev, bool new_carrier) {

    if (new_carrier)
        netif_carrier_on(dev);
    else
        netif_carrier_off(dev);
    return 0;
};

void tok_net_get_stats64(struct net_device *dev, struct rtnl_link_stats64 *stats) {
    int i;

    for_each_possible_cpu(i) {
        const struct pcpu_dstats *dstats;
        u64 tbytes, tpackets, tdropped;
        u64 rbytes, rpackets, rdropped;
        unsigned int start;

        dstats = per_cpu_ptr(dev->dstats, i);
        do {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,15,0)
            start = u64_stats_fetch_begin_irq(&dstats->syncp);
#else
            start = u64_stats_fetch_begin_bh(&dstats->syncp);
#endif
            tbytes = dstats->tx_bytes;
            tpackets = dstats->tx_packets;
            tdropped = dstats->tx_dropped;
            rbytes = dstats->rx_bytes;
            rpackets = dstats->rx_packets;
            rdropped = dstats->rx_dropped;
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,15,0)
        } while (u64_stats_fetch_retry_irq(&dstats->syncp, start));
#else
        } while (u64_stats_fetch_retry_bh(&dstats->syncp, start));
#endif

        stats->tx_bytes += tbytes;
        stats->tx_packets += tpackets;
        stats->tx_dropped += tdropped;
        stats->rx_bytes += rbytes;
        stats->rx_packets += rpackets;
        stats->rx_dropped += rdropped;
    }
};

void tok_net_set_multicast_list(struct net_device *dev) {
};

int tok_net_open(struct net_device *dev) {

    // Start the network interface queue
    DBG(KERN_INFO "toknet: Starting netif queue for device (mac=%pM)\n", dev->dev_addr);
    netif_tx_start_all_queues(dev);

    // Turn on the carrier signal
    DBG(KERN_INFO "toknet: Turning on the carrier signal for device (mac=%pM)\n", dev->dev_addr);
    netif_carrier_on(dev);

    // Success!
    return 0;
};

int tok_net_close(struct net_device* dev) {
    tok_net_t* net;
    
    // Turn off the carrier signal
    DBG(KERN_INFO "toknet: Turning off the carrier signal for device (mac=%pM)\n", dev->dev_addr);
    netif_carrier_off(dev);

    // Stop the queue
    //rtnl_lock();
    DBG(KERN_INFO "toknet: Stopping netif queue for device (mac=%pM)\n", dev->dev_addr);
    netif_tx_stop_all_queues(dev);

    /*
     * This seems to cause a crash
     * 
    // Disable the transmit
    DBG(KERN_INFO "toknet: Disabling the transmit on net_device (mac=%pM)\n", dev->dev_addr);
    netif_tx_disable(dev);
    
    // Unregister the device
    DBG(KERN_INFO "toknet: Unregistering net_device (mac=%pM)\n", dev->dev_addr);
    unregister_netdevice(dev);
     * 
    //rtnl_unlock();
     */
    
    net = (tok_net_t*)netdev_priv(dev);
    if (net != NULL) {
        if (net->encrypt_tfm != NULL) {
            crypto_free_blkcipher(net->encrypt_tfm);
            net->encrypt_tfm = NULL;
        }
    }

    // Success!
    return 0;
};

void tok_net_release(tok_net_t* net) {    
    struct net_device* dev = net->dev;
    tok_net_pipe_t* pipe;
    tok_net_pipe_t* npipe;
    
    // Loop through all the pipes that need to be destroyed
    for (pipe = tok_net_pipe_next(net, NULL); pipe != NULL; pipe = npipe) {
        npipe = tok_net_pipe_next(net, pipe);

        // Release the pipe
        list_del(&pipe->head);
        tok_net_pipe_release(pipe);
    }
    
    // If it's up then shut it down
    rtnl_lock();
    if (dev->flags & IFF_UP)
        dev_close(dev);

    // Stop the queue
    //rtnl_lock();
    DBG(KERN_INFO "toknet: Stopping the receive queue on net_device (mac=%pM)\n", dev->dev_addr);
    netif_stop_queue(dev);

    // Disable the transmit
    DBG(KERN_INFO "toknet: Disabling the transmit on net_device (mac=%pM)\n", dev->dev_addr);
    netif_tx_disable(dev);

    // Unregister the device
    DBG(KERN_INFO "toknet: Unregistering net_device (mac=%pM)\n", dev->dev_addr);
    unregister_netdevice(dev);
    rtnl_unlock();
};

struct rtnl_link_ops tok_net_link_ops __read_mostly = {
    .kind = "vnp",
    .setup = tok_net_setup
};

/*
 * This function is invoked when a packet is received on the virtual NIC
 * 
 * See this for details: https://tools.ietf.org/html/rfc2473
 */
netdev_tx_t tok_net_xmit(struct sk_buff* rcv, struct net_device* dev) {

    // Declare variables
    tok_net_t* net;
    int ret = FALSE;
    struct pcpu_dstats* dstats;
    
#ifdef TOK_ENABLE_SKBUFF_FLAGS
    // Make sure the affinity is recorded for future steering
    // (but not if its a reply packet which needs to use the source CPU
    //  that was recorded in the packet earlier)
    if ((rcv->tok_flags & TOK_SKB_FLAG_REPLY) == 0)
        rcv->affinity = raw_smp_processor_id();
#endif

    // We either encapsulate or decapsulate
    net = (tok_net_t*) netdev_priv(dev);
    switch (net->flags.wrap_mode) {

        case tok_net_wrap_mode_encapsulate_ipv6:
        case tok_net_wrap_mode_encapsulate_ether:
        {
            // Encapsulate the packet and receive the size of the new packet
            ret = tok_net_xmit_encapsulate(rcv, dev, net);

            // Update the stats
            dstats = this_cpu_ptr(dev->dstats);
            u64_stats_update_begin(&dstats->syncp);
            if (ret == TRUE) {
                dstats->tx_packets++;                
                dstats->tx_bytes += rcv->len;
            }
            else
                dstats->tx_dropped++;
            u64_stats_update_end(&dstats->syncp);
            break;
        }
        default:
        {
            kfree_skb(rcv);

            // Update the stats
            dstats = this_cpu_ptr(dev->dstats);
            u64_stats_update_begin(&dstats->syncp);
            dstats->tx_dropped++;
            u64_stats_update_end(&dstats->syncp);
            break;
        }
    }

    // We are finished
    return NETDEV_TX_OK;
};

const struct net_device_ops tok_net_ops = {
    .ndo_init = tok_net_init,
    .ndo_uninit = tok_net_uninit,
    .ndo_open = tok_net_open,
    .ndo_stop = tok_net_close,
    .ndo_start_xmit = tok_net_xmit,
    .ndo_change_carrier = tok_net_change_carrier,
    .ndo_get_stats64 = tok_net_get_stats64,
    .ndo_set_rx_mode = tok_net_set_multicast_list,
    .ndo_validate_addr = eth_validate_addr,
    .ndo_set_mac_address = eth_mac_addr
};

/*
 * Gets a pipe by its particular index (wrapping around if the index is out of bounds)
 */
tok_net_pipe_t* tok_net_get_pipe(tok_net_t* net, uint32_t index) {
    
    // Declare variables
    tok_net_pipe_t*     pipe;
    uint32_t            pipe_count = 0;
    uint32_t            n;
        
    // Compute the pipe count
    pipe = tok_net_pipe_next(net, NULL);
    for (;pipe != NULL; pipe = tok_net_pipe_next(net, pipe)) {
        if (pipe->weight == 0)
            continue;
        pipe_count++;
    }
    
    // Perform a quick validation
    if (pipe_count <= 0)
        return tok_net_pipe_next(net, NULL);
    
    // Project the index into the pipe space
    index = index % pipe_count;
    
    // Find the pipe that meets this index
    pipe = tok_net_pipe_next(net, NULL);
    for (n = 0; pipe != NULL; pipe = tok_net_pipe_next(net, pipe)) {
        if (pipe->weight == 0)
            continue;
        if (n >= index)
            return pipe;
        n++;
    }
    
    // If no pipe was found then just return the first pipe
    pipe = tok_net_pipe_next(net, NULL);
    for (;pipe != NULL; pipe = tok_net_pipe_next(net, pipe)) {
        if (pipe->weight != 0)
            return pipe;
    }
    return tok_net_pipe_next(net, NULL);
}

static inline loff_t readlba(u8 *lba) {
    loff_t n = 0ULL;
    int i;

    for (i = 5; i >= 0; i--) {
        n <<= 8;
        n |= lba[i];
    }
    return n;
}

/*
 * Gets a pipe by its particular index (wrapping around if the index is out of bounds)
 */
tok_balance_t tok_net_balance_pipe(tok_net_t* net, struct sk_buff* skb, int seq) {
    
    // Declare variables
    tok_balance_t           ret;
    struct ethhdr*          eth;
    struct iphdr*           ip4;
    struct ipv6hdr*         ip6;
    struct ipv6_opt_hdr*    opt;
    struct udphdr*          udp;
    struct tcphdr*          tcp;
    struct aoe_hdr*         aoe;
    struct aoe_atahdr*      ata;
#ifdef TOK_ENABLE_SKBUFF_FLAGS
    uint64_t                crc = net->lb_iv ^ skb->affinity;
#else
    uint64_t                crc = net->lb_iv;
#endif
    int                     offset = 0;
    int                     nexthdr;
    
    struct {
        struct iphdr            ip4;
        struct ipv6hdr          ip6;
        struct ipv6_opt_hdr     ip6_opt;
        struct tcphdr           tcp;
        struct udphdr           udp;
        union
        {
            struct aoe_hdr      aoe;
            struct
            {
                struct ethhdr           eth;
                struct aoe_short_hdr    aoe_short;
            };
        };
        struct aoe_atahdr       ata;
    } __attribute__((packed)) cache;
    
    // Read the Ethernet header
    // (if this fails then just dump the traffic down the first pipe)
    eth = eth_hdr(skb);
    if (unlikely(eth == NULL))
        goto hard_balance;
    
#ifdef TOK_ENABLE_SKBUFF_FLAGS
    // If we are a reply packet then don't bother trying to do any load balancing
    // as it will interfere with the packet steering
    if ((skb->tok_flags & TOK_SKB_FLAG_REPLY) != 0)
        goto hard_balance;
#endif
    
    // If the packet is a AOE disk IO packet then it can operate completely in parallel
    // as packets don't need to be received in order of transmission (however we should
    // group them together into bunches so that IO grouping can take place)
    if (eth->h_proto == htons(ETH_P_AOE))
    {
#ifdef TOK_ENABLE_ENCRYPTION
        // If encryption is enabled then we should spread the load across
        // multiple cores so that it doesn't consume all the resources
        // -------------------------------
        // If the packet is a AOE disk IO packet then it can operate completely in parallel
        // as packets don't need to be received in order of transmission (however we should
        // group them together into bunches so that IO grouping can take place)
        if (net->flags.encrypt_mode == tok_net_encrypt_mode_encrypt &&
            likely(pskb_may_pull(skb, sizeof(struct aoe_short_hdr) + sizeof(struct aoe_atahdr))))
        {
            aoe = (struct aoe_hdr*)eth_hdr(skb);
            if (aoe->cmd == AOECMD_ATA) {
                ata = (struct aoe_atahdr*)&aoe->data;

                ret.flow_id = (readlba(ata->lba) + ata->scnt) / 128;
                ret.parallel = 1;
                ret.pipe = tok_net_get_pipe(net, ret.flow_id);
                return ret;
            }
        }
        else
            goto hard_balance;
#else
        goto hard_balance;
#endif
    }
    
    // If its an IP4 packet then we can make a more intelligent decision on how
    // to load balance using the information in the packet headers
    else if (eth->h_proto == htons(ETH_P_IP))
    {
        if (unlikely(offset + sizeof (struct iphdr) > skb->len)) goto hard_balance;
        ip4 = skb_header_pointer(skb, offset, sizeof (struct iphdr), &cache.ip4);
        if (unlikely(ip4 == NULL)) goto hard_balance;
        offset += ip4->ihl*4;
        
        // Otherwise use the IP4 packet for load balancing information
        crc = tok_crc64(crc, &ip4->saddr, sizeof(__be32));
        crc = tok_crc64(crc, &ip4->daddr, sizeof(__be32));

        // If its a UDP or ICMP packet then we can use the more optimal round robin load balancing
        // as these types of packets are not guaranteed to be delivered in transmission order further
        // they are unreliable thus no fast retransmission algorithms are being used (such as TCP fast retransmit)
        if (ip4->protocol == IPPROTO_UDP)
        {
            if (unlikely(offset + sizeof (struct udphdr) > skb->len)) goto hard_balance;
            udp = skb_header_pointer(skb, offset, sizeof (struct udphdr), &cache.udp);
            if (unlikely(udp == NULL)) goto hard_balance;
            offset += sizeof(struct udphdr);
            
            crc = tok_crc64(crc, &udp->source, sizeof(__be16));
            crc = tok_crc64(crc, &udp->dest, sizeof(__be16));
        }

        // If its a TCP connection then we can do some additional work here
        if (ip4->protocol == IPPROTO_TCP)
        {
            if (unlikely(offset + sizeof (struct tcphdr) > skb->len)) goto hard_balance;
            tcp = skb_header_pointer(skb, offset, sizeof (struct tcphdr), &cache.tcp);
            if (unlikely(tcp == NULL)) goto hard_balance;
            offset += sizeof(struct tcphdr);
            
            crc = tok_crc64(crc, &tcp->source, sizeof(__be16));
            crc = tok_crc64(crc, &tcp->dest, sizeof(__be16));
        }

        // Move execution to the end where additional entropy will be added to the checksum
        // we have already done using data in the Ethernet header
        goto hard_balance;
    }
    
    // If its an IP6 packet then we can make a more intelligent decision on how
    // to load balance using the information in the packet headers
    else if (eth->h_proto == htons(ETH_P_IPV6))
    {
        if (unlikely(offset + sizeof (struct ipv6hdr) > skb->len)) goto hard_balance;        
        ip6 = skb_header_pointer(skb, offset, sizeof (struct ipv6hdr), &cache.ip6);
        if (unlikely(ip6 == NULL)) goto hard_balance;
        offset += sizeof(struct ipv6hdr);
        
        // Otherwise use the IP4 packet for load balancing information
        crc = tok_crc64(crc, &ip6->saddr, sizeof(struct in6_addr));
        crc = tok_crc64(crc, &ip6->daddr, sizeof(struct in6_addr));
           
        // Evaluate all the headers and payload
        nexthdr = ip6->nexthdr;
        while (likely(offset < skb->len)) {
            switch (nexthdr) {
                case NEXTHDR_HOP:
                case NEXTHDR_ROUTING:
                case NEXTHDR_DEST:
                    if (unlikely(offset + sizeof (struct ipv6_opt_hdr) > skb->len)) goto hard_balance;        
                    opt = skb_header_pointer(skb, offset, sizeof(struct ipv6_opt_hdr), &cache.ip6_opt);
                    if (unlikely(opt == NULL)) goto hard_balance;
                    offset += ipv6_optlen(opt);
                    nexthdr = opt->nexthdr;
                    continue;
                    
                // If its a UDP or ICMP packet then we can use the more optimal round robin load balancing
                // as these types of packets are not guaranteed to be delivered in transmission order further
                // they are unreliable thus no fast retransmission algorithms are being used (such as TCP fast retransmit)
                case IPPROTO_UDP:
                case IPPROTO_UDPLITE:
                case IPPROTO_ICMP:
                case IPPROTO_ICMPV6:
                {
                    if (unlikely(offset + sizeof (struct udphdr) > skb->len)) goto hard_balance;
                    udp = skb_header_pointer(skb, offset, sizeof (struct udphdr), &cache.udp);
                    if (unlikely(udp == NULL)) goto hard_balance;
                    offset += sizeof(struct udphdr);

                    crc = tok_crc64(crc, &udp->source, sizeof(__be16));
                    crc = tok_crc64(crc, &udp->dest, sizeof(__be16));
                    goto hard_balance;
                }
                    
                // If its a TCP packet
                case IPPROTO_TCP:
                {
                    if (unlikely(offset + sizeof (struct tcphdr) > skb->len)) goto hard_balance;  
                    tcp = skb_header_pointer(skb, offset, sizeof (struct tcphdr), &cache.tcp);
                    if (unlikely(tcp == NULL)) goto hard_balance;
                    offset += sizeof(struct tcphdr);

                    crc = tok_crc64(crc, &tcp->source, sizeof(__be16));
                    crc = tok_crc64(crc, &tcp->dest, sizeof(__be16));
                    goto hard_balance;
                }
                    
                // Otherwise we are finished looking for anything else to balance on
                default:
                    goto hard_balance;
            }
        }

        // If we get here then the packet is corrupt or holds optional headers that
        // are not recognized therefore we should just balance as best we can using just
        // the IPv6 and Ethernet headers (which will perform quite well anyway)
        goto hard_balance;
    }
    
hard_balance:
    // Otherwise just load balance based on the source and destination MAC addresses
    crc = tok_crc64(crc, eth, sizeof(struct ethhdr));
    ret.flow_id = (uint32_t)crc;
    ret.parallel = 0;
    ret.pipe = tok_net_get_pipe(net, ret.flow_id);
    return ret;
}

/*
 * Process a packet (either in an interupt or not)
 */
static int tok_netif_rx(struct sk_buff* skb) {
    
    if (in_interrupt())
        return netif_rx(skb);
    else
        return netif_rx_ni(skb);
}

/*
 * See this for details: https://tools.ietf.org/html/rfc2473
 */
int tok_net_xmit_encapsulate(struct sk_buff* skb, struct net_device* dev, tok_net_t* net) {
    // Declare variables
    struct net_device*  target;
    tok_net_pipe_t*     pipe;
    int                 padding;
    struct ipv6hdr*     ip;
    struct ethhdr*      eth;
    toknethdr_t*        esp;
    int                 seq;
    int                 cpu;
    int                 len = 0;
    int                 hlen;
    struct pcpu_dstats* dstats;
    int                 proto;
    tok_balance_t       balance;
    tok_thread_t*       t;
    int                 orig_len = skb->len;
    
    // Increment the sequence number in the local thread
    // (this stops cache line thrashing)
    cpu = get_cpu();
    t = (tok_thread_t*)per_cpu_ptr(root.thread_percpu, cpu);
    seq = t->seq++;
    put_cpu();
    
    // Turn the packet into a linear buffer that is much easier to deal with
    // in the later stages of the processing
    skb = skb_share_check(skb, GFP_ATOMIC);
    if (unlikely(skb == NULL)) {
        kfree_skb(skb);
        return FALSE;
    }
        
    // Grab the pipe that represents the current index location
    balance = tok_net_balance_pipe(net, skb, seq);
    pipe = balance.pipe;
    if (unlikely(pipe == NULL)) {
        consume_skb(skb);
        return FALSE;
    }
    target = rcu_dereference_raw_notrace(pipe->dev);

    // Make sure the pipe currently has a device attached to it
    if (unlikely(target == NULL)) {
        consume_skb(skb);
        return FALSE;
    }
    
    //DBG(KERN_INFO "PKT-OUT! (pipe.src=%pM, pipe.dst=%pM, wrap=%d, devname=%s, portname=%s)\n", &pipe->src_mac, &pipe->dest_mac, net->flags.wrap_mode, target->name, net->dev->name);
    
    // Scrub the packet and assign it to the others
    skb_scrub_packet(skb, true);
    
    // Compute the head room required and align it with the AES_BLOCK_SIZE boundary
    hlen = ETH_HLEN;
    if (net->flags.wrap_mode == tok_net_wrap_mode_encapsulate_ipv6)
        hlen += sizeof (struct ipv6hdr);
    hlen += sizeof(toknethdr_t);
    
    // Make sure there is head room (if not it will be expanded
    if (skb_headroom(skb) < hlen) {
        if (pskb_expand_head(skb, hlen, 0, GFP_ATOMIC)) {
            kfree_skb(skb);
            return FALSE;
        }
    }
    
    // Add the ESP header in front of the payload
    esp = (toknethdr_t*)skb_push(skb, sizeof(toknethdr_t));
    if (esp == NULL) {
        kfree_skb(skb);
        return FALSE;
    }
    esp->u.segment = net->seg;
#ifdef TOK_ENABLE_ENCRYPTION
    esp->u.encrypted = net->flags.encrypt_mode == tok_net_encrypt_mode_encrypt ? 1 : 0;
#else
    esp->u.encrypted = 0;
#endif
    esp->u.flow_id = balance.flow_id;
    esp->u.flow_seq = seq;
    if (balance.parallel == 1)
        esp->u.mode |= TOK_SKB_FLAG_SPREAD;
#ifdef TOK_ENABLE_SKBUFF_FLAGS
    esp->u.affinity = skb->affinity;
#else
    esp->u.affinity = 0;
#endif
    esp->br_dst = net->br_dst;
    esp->br_src = net->br_src;
    len += sizeof(toknethdr_t);
    skb_reset_transport_header(skb);
    
    //DBG(KERN_INFO "PKT-OUT! (cpu=%d, parallel=%d, flow_id=%d, flow_seq=%d)\n", cpu, balance.parallel, esp->u.flow_id, esp->u.flow_seq);

#ifdef TOK_ENABLE_ENCRYPTION
    // If encryption is enabled then encrypt the blocks
    if (esp->u.encrypted == 1)
    {
        int sg_n;
        int sg_max = skb_shinfo(skb)->nr_frags + 2;
        struct scatterlist sg_src[sg_max];

        struct blkcipher_desc desc;
        int enc_len = orig_len;
        
        union {
            toknethdr_t iv_esp;
            char iv_bytes[AES_BLOCK_SIZE];
        } iv_rand;
        
        // Build the IV using the data we already know
        memset(&iv_rand, 0, sizeof(iv_rand));
        memcpy(&iv_rand.iv_esp, esp, sizeof(toknethdr_t));
        
        // Compute the padding required to align the new packet with the AES block size
        // without doing this the encryption will fail, however the impact to unencrypted
        // packets is negligable including this calculation itself
        padding = enc_len % AES_BLOCK_SIZE;
        if (padding != 0)
            padding = AES_BLOCK_SIZE - padding;
        enc_len += padding;
        
        // Add padding onto the end of the packet
        if (padding > 0) {
            if (skb_pad(skb, padding)) {
                kfree_skb(skb);
                return FALSE;
            }
            __skb_put(skb, padding);
        }
                
        // Prepare the encryption engine
        desc.tfm = net->encrypt_tfm;
        desc.flags = 0;
        desc.info = &iv_rand.iv_bytes[0]; // This buffer will be destroyed during the encryption process
        
        // Setup the source scatterlist
        sg_init_table(sg_src, sg_max);
        sg_n = skb_to_sgvec_nomark(skb, sg_src, sizeof(toknethdr_t), enc_len);
        sg_mark_end(&sg_src[sg_n - 1]);
        
        // Run the encryption
        crypto_blkcipher_encrypt_iv(&desc, &sg_src[0], &sg_src[0], enc_len);
        len += enc_len;
    }
    else
    {
        len += orig_len;
    }
#else
    len += orig_len;
#endif

    // Add the network header
    if (net->flags.wrap_mode == tok_net_wrap_mode_encapsulate_ipv6)
    {
        // Add the IPv6 Header
        ip = (struct ipv6hdr*) skb_push(skb, sizeof (struct ipv6hdr));
        if (ip == NULL) {
            kfree_skb(skb);
            return FALSE;
        }
        ip->version = 6;
        ip->priority = 0;
        ip->flow_lbl[0] = 0;
        ip->flow_lbl[1] = 0;
        ip->flow_lbl[2] = 0;
        ip->payload_len = htons(len);
        ip->nexthdr = IPPROTO_ESP;
        ip->hop_limit = 64;
        ip->saddr.in6_u.u6_addr32[0] = pipe->src_ip.u32[0];
        ip->saddr.in6_u.u6_addr32[1] = pipe->src_ip.u32[1];
        ip->saddr.in6_u.u6_addr32[2] = pipe->src_ip.u32[2];
        ip->saddr.in6_u.u6_addr32[3] = pipe->src_ip.u32[3];
        ip->daddr.in6_u.u6_addr32[0] = pipe->dest_ip.u32[0];
        ip->daddr.in6_u.u6_addr32[1] = pipe->dest_ip.u32[1];
        ip->daddr.in6_u.u6_addr32[2] = pipe->dest_ip.u32[2];
        ip->daddr.in6_u.u6_addr32[3] = pipe->dest_ip.u32[3];
        
        proto = ETH_P_IPV6;
        len += sizeof (struct ipv6hdr);
    }
    else
        proto = ETH_P_TOKNET;
    skb_reset_network_header(skb);
    
    // Add the Ethernet header for the IPv6
    eth = (struct ethhdr*) skb_push(skb, ETH_HLEN);
    if (eth == NULL) {
        kfree_skb(skb);
        return FALSE;
    }    
    eth->h_proto = htons(proto);
    memcpy(&eth->h_source, &pipe->src_mac, ETH_ALEN);
    memcpy(&eth->h_dest, &pipe->dest_mac, ETH_ALEN);
    len += ETH_HLEN;
    skb_reset_mac_header(skb);
    
    // Prepare the packet for transmission    
    skb->mac_len = ETH_HLEN;
    skb->protocol = proto;
    skb->dev = target;
    skb->pkt_type = PACKET_HOST;
    skb->len = len;
    
    //DBG(KERN_INFO "PKT-SND0! (dev=%s, src=%pM, dst=%pM, len=%d, seq=%d)\n", skb->dev->name, &eth_hdr(skb)->h_source, &eth_hdr(skb)->h_dest, skb->len, esp->sequence);
        
    // Receive the packet back onto the machine
    if (dev_queue_xmit(skb) == NET_RX_SUCCESS)
    {
        // Update the stats
        dstats = this_cpu_ptr(target->dstats);
        u64_stats_update_begin(&dstats->syncp);
        dstats->tx_packets++;
        dstats->tx_bytes += len;
        u64_stats_update_end(&dstats->syncp);
        return TRUE;
    }
    
    // Otherwise the packet was dropped
    else
    {
        // Update the stats
        dstats = this_cpu_ptr(target->dstats);
        u64_stats_update_begin(&dstats->syncp);
        dstats->tx_dropped++;
        u64_stats_update_end(&dstats->syncp);
        return FALSE;
    }
}

u32 tok_net_always_on(struct net_device *dev) {
    return 1;
};

const struct ethtool_ops tok_net_ethtool_ops = {
    .get_link = tok_net_always_on
};

void tok_net_setup(struct net_device* dev) {

    // Set up the device for ethernet
    ether_setup(dev);

    // Set the basic properties of the device
    dev->mtu = 64 * 1024;
    dev->hard_header_len = ETH_HLEN + sizeof(struct ipv6hdr) + sizeof(toknethdr_t) + ETH_HLEN;
    dev->addr_len = ETH_ALEN;
    dev->tx_queue_len = 0;
    dev->type = ARPHRD_ETHER;
    dev->flags = IFF_BROADCAST | IFF_MULTICAST | IFF_PROMISC;

    // Set the features
    dev->hw_features = NETIF_F_ALL_TSO;
    dev->features = NETIF_F_SG | NETIF_F_FRAGLIST
            | NETIF_F_ALL_TSO
            | NETIF_F_HW_CSUM
            | NETIF_F_RXCSUM
            | NETIF_F_HIGHDMA
            | NETIF_F_LLTX
            | NETIF_F_NETNS_LOCAL
            | NETIF_F_VLAN_CHALLENGED;

    // Fill in device structure with ethernet-generic values
    dev->tx_queue_len = 0;
    eth_hw_addr_random(dev);

    // Set all the callbacks
    dev->ethtool_ops = &tok_net_ethtool_ops;
    dev->netdev_ops = &tok_net_ops;
};

uint64_t tok_net_skb_crc64(struct sk_buff* skb, int offset, int len, uint64_t csum) {
    int start = skb_headlen(skb);
    int i;
    int copy = start - offset;
    struct sk_buff *frag_iter;
    int pos = 0;

    /* Checksum header. */
    if (copy > 0) {
        if (copy > len) copy = len;

        csum = tok_crc64(csum, skb->data + offset, copy);

        if ((len -= copy) == 0) {
            return csum;
        }
        offset += copy;
        pos = copy;
    }

    for (i = 0; i < skb_shinfo(skb)->nr_frags; i++) {
        int end;
        skb_frag_t *frag = &skb_shinfo(skb)->frags[i];

        WARN_ON(start > offset + len);

        end = start + skb_frag_size(frag);
        if ((copy = end - offset) > 0) {
            u8 *vaddr;

            if (copy > len) copy = len;

            vaddr = kmap_atomic(skb_frag_page(frag));
            csum = tok_crc64(csum, vaddr + frag->page_offset + offset - start, copy);
            kunmap_atomic(vaddr);

            if ((len -= copy) == 0) {
                return csum;
            }
            offset += copy;
            pos += copy;
        }
        start = end;
    }

    skb_walk_frags(skb, frag_iter) {
        int end;

        WARN_ON(start > offset + len);

        end = start + frag_iter->len;
        if ((copy = end - offset) > 0) {
            if (copy > len) copy = len;

            csum = tok_net_skb_crc64(frag_iter, offset - start, copy, csum);

            if ((len -= copy) == 0) {
                return csum;
            }
            offset += copy;
            pos += copy;
        }
        start = end;
    }
    
    return csum;
};

void tok_net_skb_copyto_buf(struct sk_buff* skb, int offset, void* buf, int len) {
    // Declare variables
    int start = skb_headlen(skb);
    int i;
    int copy = start - offset;
    struct sk_buff *frag_iter;
    int pos = 0;
    uint8_t* pbuf = (uint8_t*) buf;
    uint8_t* map;

    // First add any linear data into the BVEC
    if (copy > 0) {
        uint8_t* data;

        if (copy > len) copy = len;
        data = skb->data + offset;

        if (copy > 0) {
            memcpy(pbuf + pos, data, copy);

            if ((len -= copy) == 0) {
                return;
            }
            offset += copy;
            pos += copy;
        }
    }

    // Add the pages the packet has referenced to the BVEC
    for (i = 0; i < skb_shinfo(skb)->nr_frags; i++) {
        int end;
        skb_frag_t *frag = &skb_shinfo(skb)->frags[i];

        WARN_ON(start > offset + len);

        end = start + skb_frag_size(frag);
        if ((copy = end - offset) > 0) {
            if (copy > len) copy = len;

            if (copy > 0) {
                map = (uint8_t*) kmap(frag->page.p);
                memcpy(pbuf + pos, map + frag->page_offset, copy);
                kunmap(frag->page.p);

                if ((len -= copy) == 0) {
                    return;
                }
                offset += copy;
                pos += copy;
            }
        }
        start = end;
    }

    // Add any packet fragments that were received to the BVEC

    skb_walk_frags(skb, frag_iter) {
        int end;

        WARN_ON(start > offset + len);

        end = start + frag_iter->len;
        if ((copy = end - offset) > 0) {
            if (copy > len) copy = len;

            tok_net_skb_copyto_buf(frag_iter, offset - start, pbuf + pos, copy);

            if ((len -= copy) == 0) {
                return;
            }
            offset += copy;
            pos += copy;
        }
        start = end;
    }

    // While there is still more remaining just keep adding the zero page to the BVEC to pad it out with zeros
    while (len > 0) {
        copy = len;

        memset(pbuf + pos, 0, copy);

        if ((len -= copy) == 0) {
            return;
        }
        offset += copy;
        pos += copy;
    }
};

tok_net_t* tok_net_next_list(struct list_head* list, tok_net_t* last) {
    // If the last one exists then try the next
    if (last != NULL) {
        // If the next is the end then exit
        if (rcu_dereference_raw_notrace(last->head.next) == NULL ||
                rcu_dereference_raw_notrace(last->head.next) == list) {
            return NULL;
        }

        // Return the next
        return container_of(rcu_dereference_raw_notrace(last->head.next), tok_net_t, head);
    }

    // Validate this is not an empty list
    if (rcu_dereference_raw_notrace(list->next) == NULL ||
            rcu_dereference_raw_notrace(list->next) == list) {
        return NULL;
    }

    return container_of(rcu_dereference_raw_notrace(list->next), tok_net_t, head);
};

tok_net_t* tok_net_next(tok_net_t* last) {
    return tok_net_next_list(&root.net_list, last);
};

void tok_net_pipe_init(tok_net_pipe_t* pipe) {

    memset(pipe, 0, sizeof (tok_net_pipe_t));
};

tok_net_pipe_t* tok_net_pipe_next_list(struct list_head* list, tok_net_pipe_t* last) {

    // If the last one exists then try the next
    if (last != NULL) {
        // If the next is the end then exit
        if (rcu_dereference_raw_notrace(last->head.next) == NULL ||
                rcu_dereference_raw_notrace(last->head.next) == list) {
            return NULL;
        }

        // Return the next
        return container_of(rcu_dereference_raw_notrace(last->head.next), tok_net_pipe_t, head);
    }

    // Validate that this is not an empty list
    if (rcu_dereference_raw_notrace(list->next) == NULL ||
            rcu_dereference_raw_notrace(list->next) == list) {
        return NULL;
    }

    return container_of(rcu_dereference_raw_notrace(list->next), tok_net_pipe_t, head);
};

tok_net_pipe_t* tok_net_pipe_next_list_del(struct list_head* list, tok_net_pipe_t* last) {

    // If the last one exists then try the next
    if (last != NULL) {
        // If the next is the end then exit
        if (last->del.next == NULL ||
                last->del.next == list) {
            return NULL;
        }

        // Return the next
        return container_of(last->del.next, tok_net_pipe_t, del);
    }

    // Validate that this is not an empty list
    if (list->next == NULL ||
            list->next == list) {
        return NULL;
    }

    return container_of(list->next, tok_net_pipe_t, del);
};

tok_net_pipe_t* tok_net_pipe_next(tok_net_t* net, tok_net_pipe_t* last) {
    return tok_net_pipe_next_list(&net->pipes, last);
};

tok_net_pipe_t* tok_net_pipe_create(void) {
    tok_net_pipe_t* pipe;

    // Create the network device pipe (using the cache)
    pipe = (tok_net_pipe_t*) kmem_cache_alloc_node(root.net_pipe_cache, GFP_ATOMIC, numa_node_id());
    if (pipe == NULL) {
        return NULL;
    }
    tok_net_pipe_init(pipe);

    return pipe;
};

void tok_net_pipe_release(tok_net_pipe_t* pipe) {

    kmem_cache_free(root.net_pipe_cache, pipe);
};

void tok_net_refresh_pointers(void) {
    // Declare variables
    tok_net_t* net;
    tok_net_t* target;
    tok_net_pipe_t* pipe;
    struct net_device *nd;
    struct net* ns;

    // Loop through all the network devices
    rcu_read_lock();
    net = tok_net_next(NULL);
    for (; net != NULL; net = tok_net_next(net)) {

        // Now loop through all the pipes
        pipe = tok_net_pipe_next(net, NULL);
        for (; pipe != NULL; pipe = tok_net_pipe_next(net, pipe)) {

            // Scan for the target device
            target = tok_net_next(NULL);
            for (; target != NULL; target = tok_net_next(target)) {
                if (strncmp(target->devname, pipe->devname, IFNAMSIZ) == 0)
                    break;
            }

            // Update the device target
            if (target != NULL)
                rcu_assign_pointer(pipe->dev, target->dev);
            else
            {
                // Ok, we didnt find the device in our list so lets just try looking it
                // up by the name itself
                nd = dev_get_by_name(&init_net, pipe->devname);
                if (nd == NULL) {
                    for_each_net_rcu(ns) {
                        nd = dev_get_by_name_rcu(ns, pipe->devname);
                        if (nd != NULL) break;
                    }
                }
                
                rcu_assign_pointer(pipe->dev, nd);
                
                if (nd != NULL)
                    dev_put(nd);
            }
        }
    }
    rcu_read_unlock();
};

static int tok_rcv(struct sk_buff *rcv, struct net_device *ndev, struct packet_type *pt, struct net_device *orig_dev)
{
    // Process the packet
    tok_process(rcv, 0);
    return 0;
}

#ifdef TOK_ENABLE_SKBUFF_FLAGS
static int tok_steer(int cpu, int index)
{
    return index % num_online_cpus();
}
#endif

static int tok_spread(int cpu, int index)
{
    int n;
    int node = numa_node_id();
    int num_cpus = 0;
    
    // Count the number of CPU's in this particular node
    for (n = 0; n < num_online_cpus(); n++) {
        if (n == cpu) continue;
        if (cpu_to_node(n) == node)
            num_cpus++;
    }
    
    // If there are no CPU's then just return the current one
    if (num_cpus <= 0)
        return cpu;
    
    // Cap the index by the num cpus
    index = index % num_cpus;
    
    // Now we need to select the CPU we are after
    for (n = 0; n < num_online_cpus(); n++ ) {
        if (n == cpu) continue;
        if (cpu_to_node(n) == node)
        {
            if (index == 0)
                return n;
            index--;
        }            
    }
    
    // We didnt find the CPU so just return the current one
    return cpu;
}

int tok_relay(struct sk_buff *skb, struct net_device* target, tok_mac_t* src, tok_mac_t* dest)
{
    struct ethhdr* eth;
    
    // We need to make sure that we are the only owner of this packet as we are about to manipulate it
    skb = skb_share_check(skb, GFP_ATOMIC);
    if (skb == NULL) {
        dev_kfree_skb(skb);
        return NET_RX_DROP;
    }
    
    // Push the Ethernet header forward again
    skb_push(skb, ETH_HLEN);
    
    // Setup other parameters
    skb_scrub_packet(skb, false);
    skb->dev = target;
    skb->pkt_type = PACKET_HOST;
    skb->ip_summed = CHECKSUM_UNNECESSARY; /* don't check it */

    // Set all the packet headers
    skb_reset_mac_header(skb);
    skb_reset_network_header(skb);
    skb_reset_transport_header(skb);
    skb_reset_mac_len(skb);

    eth = (struct ethhdr *) skb_mac_header(skb);
    memcpy(eth->h_source, src, ETH_ALEN);
    memcpy(eth->h_dest, dest, ETH_ALEN);
    
    if (unlikely(!pskb_may_pull(skb, ETH_HLEN)))
    {
        dev_kfree_skb(skb);
        return NET_RX_DROP;
    }
    
    // Receive the packet back onto the machine
    if (dev_queue_xmit(skb) == NET_RX_SUCCESS)
    {
        // Update the stats
        struct pcpu_dstats* dstats = this_cpu_ptr(target->dstats);
        u64_stats_update_begin(&dstats->syncp);
        dstats->tx_packets++;
        dstats->tx_bytes += skb->len;
        u64_stats_update_end(&dstats->syncp);
        return TRUE;
    }
    
    // Otherwise the packet was dropped
    else
    {
        // Update the stats
        struct pcpu_dstats* dstats = this_cpu_ptr(target->dstats);
        u64_stats_update_begin(&dstats->syncp);
        dstats->tx_dropped++;
        u64_stats_update_end(&dstats->syncp);
        return FALSE;
    }
}

int tok_process(struct sk_buff *rcv, int steered)
{
    // Declare variables
    struct ethhdr*          eth;
    struct ipv6hdr*         ip6 = NULL;
    toknethdr_t*            esp;
    tok_net_t*              net;
    tok_net_flags_t         net_flags;
    tok_mac_t               net_gateway_mac;
    struct crypto_blkcipher * net_encrypt_tfm;
    struct pcpu_dstats*     dstats;
    struct net_device*      target;
    int                     offset = 0;
    
    struct {
        struct ipv6hdr      ip6;
        toknethdr_t         esp;
    } __attribute__((packed)) cache;
    
    // Read and validate it
    eth = eth_hdr(rcv);
    if (eth == NULL)
        goto drop;
    
    // Read the IPv6 header and validate it
    if (eth->h_proto == htons(ETH_P_IPV6)) {
        ip6 = skb_header_pointer(rcv, offset, sizeof (struct ipv6hdr), &cache.ip6);
        if (ip6 == NULL) goto drop;
        offset += sizeof (struct ipv6hdr);
        
        if (ip6->nexthdr != IPPROTO_ESP)
            goto drop;
    }
    else if (eth->h_proto != htons(ETH_P_TOKNET))
        goto drop;
    
    //DBG(KERN_INFO "PKT-FND0! (dev=%s, src=%pM, dst=%pM, len=%d)\n", rcv->dev->name, &eth->h_source, &eth->h_dest, rcv->len);
    
    // Read the ESP header
    esp = skb_header_pointer(rcv, offset, sizeof (toknethdr_t), &cache.esp);
    if (esp == NULL) goto drop;
    offset += sizeof (toknethdr_t);
    
#ifdef TOK_ENABLE_SKBUFF_FLAGS
    // Set all the packet properties from the ESP header
    rcv->affinity = esp->u.affinity;
#endif
    
    // Loop through all the net devices and find a network device node that matches
    rcu_read_lock();
    target = NULL;
    net = tok_net_next(NULL);
    for (;net != NULL; net = tok_net_next(net)) {
        
        // Make sure the packet is for this network device
        if (net->seg != esp->u.segment ||
            net->br_dst.rep1.high != esp->br_src.rep1.high ||
            net->br_src.rep1.high != esp->br_dst.rep1.high ||
            net->br_dst.rep1.low  != esp->br_src.rep1.low ||
            net->br_src.rep1.low  != esp->br_dst.rep1.low)
            continue;
        
        //DBG(KERN_INFO "PKT-FND0b! (dev=%s, src=%pM, dst=%pM, len=%d)\n", rcv->dev->name, &eth->h_source, &eth->h_dest, rcv->len);
        
        // If we have found our target then we have found a network device
        target = rcu_dereference_raw_notrace(net->dev);
        if (target != NULL)
        {
            // Update the stats
            if (net->dev != NULL)
            {
                dstats = this_cpu_ptr(net->dev->dstats);
                u64_stats_update_begin(&dstats->syncp);
                dstats->rx_packets++;
                dstats->rx_bytes += rcv->len;
                u64_stats_update_end(&dstats->syncp);
            }
            
            // Lets move onto the rest of the processing
            net_flags.all = net->flags.all;
            net_gateway_mac = net->gateway_mac;
            net_encrypt_tfm = net->encrypt_tfm;
            break;
        }
    }
    
    //DBG(KERN_INFO "PKT-FND1! (dev=%s, src=%pM, dst=%pM, len=%d)\n", rcv->dev->name, &eth->h_source, &eth->h_dest, rcv->len);
    
    // If the packet can't be delivered locally then its still possible that it
    // needs to be relayed onwards to other nodes
    if (target == NULL)
    {        
        //DBG(KERN_INFO "PKT-REL1! (dev=%s, src=%pM, dst=%pM, len=%d)\n", rcv->dev->name, &eth->h_source, &eth->h_dest, rcv->len);
        
        // Loop through all the devices that hold information
        net = tok_net_next(NULL);
        for (;net != NULL; net = tok_net_next(net))
        {
            //DBG(KERN_INFO "PKT-REL0! (dev=%s, src=%pM, dst=%pM, len=%d)\n", rcv->dev->name, &eth->h_source, &eth->h_dest, rcv->len);
            
            if (net->seg != esp->u.segment)
                continue;
            
            // Make sure the packet is for this network device
            if (net->br_dst.rep1.high == esp->br_dst.rep1.high &&
                net->br_dst.rep1.low  == esp->br_dst.rep1.low)
            {
                // Prevent cyclic loops
                tok_net_pipe_t* pipe = tok_net_pipe_next(net, NULL);
                for (;pipe != NULL; pipe = tok_net_pipe_next(net, pipe)) {
                    if (pipe->dev == rcv->dev)
                        break;
                }
                if (pipe != NULL) continue;
                
                // Grab the pipe that represents the current index location
                pipe = tok_net_get_pipe(net, esp->u.flow_id);
                if (pipe != NULL && pipe->dev != NULL)
                {
                    // Leave the critical section after pushing the values
                    struct net_device* other_dev = pipe->dev;
                    tok_mac_t other_src_mac = pipe->src_mac;
                    tok_mac_t other_dst_mac = pipe->dest_mac;
                    rcu_read_unlock();
                    
                    // The next function will take ownership of the packet
                    tok_relay(rcv, other_dev, &other_src_mac, &other_dst_mac);
                    return NET_RX_SUCCESS;
                }
            }
        }
        
        //DBG(KERN_INFO "PKT-REL3! (dev=%s, src=%pM, dst=%pM, len=%d)\n", rcv->dev->name, &eth->h_source, &eth->h_dest, rcv->len);
        
        // Ok we didn't relay the packet so now its time to drop it
        rcu_read_unlock();
        goto drop;
    }
    rcu_read_unlock();
    
#ifdef TOK_ENABLE_SKBUFF_FLAGS
    // We only perform packet steering once 
    if (steered == 0)
    {
        // Declare variables
        tok_thread_t* t;
        
        // First lets find the CPU that we want the packet to be sent too
        // (we dont steer if we are in an interrupt)
        int cpu = get_cpu();
        int where = cpu;
        if ((esp->u.mode & TOK_SKB_FLAG_REPLY) != 0 &&
             !in_interrupt())
        {
            where = tok_steer(cpu, esp->u.affinity);
            
            //if (cpu != where) {
            //    DBG(KERN_INFO "PKT-STEER!  (from=%d, to=%d, aff=%d)\n", cpu, where, esp->u.affinity);
            //}
        }
        else if ((esp->u.mode == TOK_SKB_FLAG_SPREAD) != 0)
        {
            where = tok_spread(cpu, esp->u.flow_id);
            //if (cpu != where) {
            //    DBG(KERN_INFO "PKT-SPREAD! (from=%d, to=%d, flow_id=%d, flow_seq=%d)\n", cpu, where, esp->u.flow_id, esp->u.flow_seq);
            //}
        }
                
        // If its not on the right CPU then send it to the right place
        if (cpu != where) {
            put_cpu();
            
            t = (tok_thread_t*)per_cpu_ptr(root.thread_percpu, where);
            skb_queue_tail(&t->skb_inq, rcv);
            wake_up_process(t->task);
            return NET_RX_SUCCESS;
        }
        put_cpu();
    }
#endif
    
    //DBG(KERN_INFO "PKT-FND2! (dev=%s, src=%pM, dst=%pM, len=%d)\n", rcv->dev->name, &eth->h_source, &eth->h_dest, rcv->len);
    
    // We need to make sure that we are the only owner of this packet as we are about to manipulate it
    // by decrypting it and stripping off the headers
    rcv = skb_share_check(rcv, GFP_ATOMIC);
    if (rcv == NULL)
        return NET_RX_DROP;

#ifdef TOK_ENABLE_ENCRYPTION    
    // Now we need to decrypt the packet
    if (esp->u.encrypted == 1)
    {
        int sg_n;
        int sg_max = skb_shinfo(rcv)->nr_frags + 2;
        struct scatterlist sg_src[sg_max];
        struct blkcipher_desc desc;
        int len = rcv->len - offset;
        
        union {
            toknethdr_t iv_esp;
            char iv_bytes[AES_BLOCK_SIZE];
        } iv_rand;
    
        // If its encrypted and in an interrupt then we need
        // to move the processing over to a background thread
        if (in_interrupt()) {
            tok_thread_t* t = (tok_thread_t*)per_cpu_ptr(root.thread_percpu, get_cpu());
            skb_queue_tail(&t->skb_inq, rcv);
            if (t->task->state == TASK_INTERRUPTIBLE)
                wake_up_process(t->task);
            put_cpu();
            return NET_RX_SUCCESS;
        }
        
        //DBG(KERN_INFO "PKT-FND3a! (dev=%s, src=%pM, dst=%pM, len=%d)\n", rcv->dev->name, &eth->h_source, &eth->h_dest, rcv->len);
        
        // Build the IV using the data we already know
        memset(&iv_rand, 0, sizeof(iv_rand));
        memcpy(&iv_rand.iv_esp, esp, sizeof(toknethdr_t));
        
        // Create the source scatterlist from the received packet
        sg_init_table(sg_src, sg_max);
        sg_n = skb_to_sgvec_nomark(rcv, sg_src, offset, len);
        if (sg_n <= 0) {
            //DBG(KERN_INFO "PKT-FND3e! (dev=%s, src=%pM, dst=%pM, len=%d)\n", rcv->dev->name, &eth->h_source, &eth->h_dest, rcv->len);
            goto drop;
        }
        sg_mark_end(&sg_src[sg_n - 1]);
        
        // Decrypt the data
        desc.flags = 0;
        desc.info = &iv_rand.iv_bytes[0];
        desc.tfm = net_encrypt_tfm;
        
        if (crypto_blkcipher_decrypt_iv(&desc, &sg_src[0], &sg_src[0], len) != 0) {
            //DBG(KERN_INFO "PKT-FND3g! (dev=%s, src=%pM, dst=%pM, len=%d)\n", rcv->dev->name, &eth->h_source, &eth->h_dest, rcv->len);
            goto drop;
        }
        
        // Adjust the packet size back down by removing the padding
        // TODO: Not sure how to do this yet or if its required
        
        //DBG(KERN_INFO "PKT-FND3b! (dev=%s, src=%pM, dst=%pM, len=%d)\n", rcv->dev->name, &eth->h_source, &eth->h_dest, rcv->len);
    }
    else
    {
        //DBG(KERN_INFO "PKT-FND3c! (dev=%s, src=%pM, dst=%pM, len=%d)\n", rcv->dev->name, &eth->h_source, &eth->h_dest, rcv->len);
        
        // For security reasons, if the inbound packets was not encrypted then we need to do some checks to
        // ensure that this is allowed under the conditions that it was received
        if (net_flags.global_encryption == TRUE ||
            net_flags.local_encryption == TRUE)
        {        
            // First we need to determine if the packet came from outside or not as this will determine how we handle the dropping
            // of this packet, determining external ingress can be done by comparing the MAC addresses
            if (memcmp(&eth->h_source, &net_gateway_mac, ETH_ALEN) == 0)
            {
                // Check the global encryption flag, if its set then we need to drop this packet
                if (net_flags.global_encryption == TRUE)
                    goto drop;
            }

            // Otherwise this is a peer-to-peer networking packet and any enabled plain text endpoint means we should allow this
            // packet in (as the security threat is the same once MAC spoofing is possible after an intrusion into a network)
            else
            {
                // Check the local encryption flag, if its set then we need to drop this packet
                if (net_flags.local_encryption == TRUE)
                    goto drop;
            }
        }
    }
#endif
    
    // Pull off all the encapsulation headers
    if (ip6 != NULL)
    {
        if (unlikely(!pskb_may_pull(rcv, sizeof(struct ipv6hdr) + sizeof(toknethdr_t) + ETH_HLEN)))
            goto drop;
        skb_pull(rcv, sizeof(struct ipv6hdr));
    }
    else
    {
        if (unlikely(!pskb_may_pull(rcv, sizeof(toknethdr_t) + ETH_HLEN)))
            goto drop;
    }
    skb_pull(rcv, sizeof(toknethdr_t));
    
    // Setup other parameters
    skb_scrub_packet(rcv, true);
    rcv->dev = target;

    // Set all the packet headers
    skb_reset_mac_header(rcv);
    skb_reset_network_header(rcv);
    skb_reset_transport_header(rcv);
    skb_reset_mac_len(rcv);
    
    // Update the protocol
    rcv->protocol = eth_type_trans(rcv, target);
    rcv->ip_summed = CHECKSUM_UNNECESSARY; /* don't check it */
    
    // Pass the packet on for processing by the kernel
    if (tok_netif_rx(rcv) == NET_RX_SUCCESS)
    {
        //DBG(KERN_INFO "PKT-FND4a! (dev=%s, src=%pM, dst=%pM, len=%d)\n", rcv->dev->name, &eth->h_source, &eth->h_dest, rcv->len);
        
        // Update the stats
        dstats = this_cpu_ptr(target->dstats);
        u64_stats_update_begin(&dstats->syncp);
        dstats->rx_packets++;
        dstats->rx_bytes += rcv->len;
        u64_stats_update_end(&dstats->syncp);
    }
    else
    {
        //DBG(KERN_INFO "PKT-FND4b! (dev=%s, src=%pM, dst=%pM, len=%d)\n", rcv->dev->name, &eth->h_source, &eth->h_dest, rcv->len);
        
        // Update the stats
        dstats = this_cpu_ptr(target->dstats);
        u64_stats_update_begin(&dstats->syncp);
        dstats->rx_dropped++;
        u64_stats_update_end(&dstats->syncp);
    }
    
    // Consume the original encapsulated packet and return to the caller
    return NET_RX_SUCCESS;
drop:
    kfree_skb(rcv);
    return NET_RX_DROP;
};

struct packet_type tok_pt_net = {
    .type = __constant_htons(ETH_P_TOKNET),
    .func = tok_rcv,
};

struct packet_type tok_pt_ip6 = {
    .type = __constant_htons(ETH_P_IPV6),
    .func = tok_rcv,
};
