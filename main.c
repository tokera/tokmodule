#include "main.h"

// Initial tokdisk structure
struct tokroot root = { };

#define KWORK_NICE          -5          // Nice value when the worker thread is scheduled or working

/*---------Module Functions -----------------*/

static int kthread_work(tok_thread_t* t) {
    struct sk_buff* iskb;
    struct sk_buff* oskb;
    int ret = 0;
    
    do {
        if ((iskb = skb_dequeue(&t->skb_inq))) {
            tok_process(iskb, 1);
            ret = 1;
        }
        if ((oskb = skb_dequeue(&t->skb_outq))) {
            dev_queue_xmit(oskb);
            ret = 1;
        }

    } while (iskb || oskb);
    
    return ret;
}

static int kthread(void* data) {
    int work;
    tok_thread_t* t = (tok_thread_t*)data;
    
    sigset_t blocked;
    
    skb_queue_head_init(&t->skb_inq);
    skb_queue_head_init(&t->skb_outq);
    
#ifdef PF_NOFREEZE
    current->flags |= PF_NOFREEZE;
#endif
    set_user_nice(current, KWORK_NICE);
    sigfillset(&blocked);
    sigprocmask(SIG_BLOCK, &blocked, NULL);
    flush_signals(current);
    complete(&t->rendez);
    
    do {
        // When we are woken, the most important thing is to get straight
        // into working (this is on the critical latency path)
        work = kthread_work(t);
        
        // Enter a loop that processes work continuous until no work
        // is found up after a complete schedule
        set_current_state(TASK_RUNNING);
        for (; work == 1 && !kthread_should_stop();)
        {
            // Schedule
            schedule();
            
            // Now check for more work
            // (if none is found then we'll go into sleep mode)
            work = kthread_work(t);
        }
        
        // We have to process work after setting INTERRUPTIBLE or we risk missing a
        // wake up in between
        set_current_state(TASK_INTERRUPTIBLE);
        work = kthread_work(t);
        
        // If work was not found then we are good to go into a sleep operation
        // (which may be really short if the task is woker again via wake_up_process)
        if (work == 0 && !kthread_should_stop())
        {
            // Scheduling while in an INTERRUPTIBLE state will cause this
            // worker thread to go to sleep
            schedule();
        }
        
    } while (!kthread_should_stop());
    
    skb_queue_purge(&t->skb_outq);
    skb_queue_purge(&t->skb_inq);
    
    __set_current_state(TASK_RUNNING);
    complete(&t->rendez);
    
    return 0;
}

/*
 * Function called when the Tokera virtual image driver is unloaded from the kernel
 */
static void cleanup_kproc(void)
{    
    int n;
    tok_thread_t* t;
    
    // Unregister the packet receive hook
    dev_remove_pack(&tok_pt_net);
    dev_remove_pack(&tok_pt_ip6);
    
    // Close all the processing threads
    for (n = 0; n < num_online_cpus(); n++)
    {
        t = (tok_thread_t*)per_cpu_ptr(root.thread_percpu, n);
        kthread_stop(t->task);
        wait_for_completion(&t->rendez);
    }
    
    // Force all the RCU callbacks to be invoked
    rcu_barrier();
    
    // Destroy all the thread memory
    if (root.thread_percpu != NULL) {
        free_percpu(root.thread_percpu);
        root.thread_percpu = NULL;
    }
    
    // Destroy the communication char device
    if (root.misc_net_registered == TRUE) {
        printk(KERN_INFO "toknet: Unregistering the base character toknet device\n");
        misc_deregister(&tok_chr_net_dev);
        root.misc_net_registered = FALSE;
    }
    
    // Destroy the cache
    if (root.net_cache != NULL)
    {
        printk(KERN_INFO "toknet: Destroying the net cache\n");
        kmem_cache_destroy(root.net_cache);
        root.net_cache = NULL;
    }
    
    // Destroy the cache
    if (root.net_pipe_cache != NULL)
    {
        printk(KERN_INFO "toknet: Destroying the net pipe cache\n");
        kmem_cache_destroy(root.net_pipe_cache);
        root.net_pipe_cache = NULL;
    }
    
    // Output to console
    printk(KERN_INFO "toknet: Kernel module has been unloaded\n");
};

/*
 * Function called when the Tokera virtual image driver is loaded into the kernel
 */
static int init_kproc(void)
{
    int n;
    int cpu_factor;
    tok_thread_t* t;
    
    printk(KERN_INFO "toknet: Device driver has been loaded\n" );
    
    // Initialize all the spinlocks
    memset(&root, 0, sizeof(struct tokroot));
    spin_lock_init(&root.sys_lock);
    
    // Initialize the list heads
    INIT_LIST_HEAD(&root.net_list);
    
    root.thread_percpu = (tok_thread_t*)alloc_percpu(struct tok_thread);
    if (root.thread_percpu == NULL)
    {
        printk(KERN_CRIT "toknet: Failed to create the thread_percpu struct");
        cleanup_kproc();
        return -ENOMEM;
    }
    
    // Allocate a zero page
    root.zero_page = virt_to_page(get_zeroed_page(GFP_KERNEL));
    
    // Compute the CPU factor
    cpu_factor = 2;
    for (;cpu_factor < num_online_cpus();)
        cpu_factor *= 2;
    
    // Register the net slab allocator
    root.net_cache = kmem_cache_create("tok_net", sizeof(struct tok_net), __alignof__(struct tok_net), SLAB_HWCACHE_ALIGN, NULL);
    if (root.net_cache == NULL)
    {
        printk(KERN_CRIT "toknet: Failed to create the kernel member cache (tok_net)");
        cleanup_kproc();
        return -ENOMEM;
    }
    
    // Register the net pipe slab allocator
    root.net_pipe_cache = kmem_cache_create("tok_net_pipe", sizeof(struct tok_net_pipe), __alignof__(struct tok_net_pipe), SLAB_HWCACHE_ALIGN, NULL);
    if (root.net_pipe_cache == NULL)
    {
        printk(KERN_CRIT "toknet: Failed to create the kernel member cache (tok_net_pipe)");
        cleanup_kproc();
        return -ENOMEM;
    } 
    
    // Create the device used to communicate with char devices
    if (misc_register(&tok_chr_net_dev))
    {
        printk(KERN_CRIT "toknet: Can't register toknet device\n");
        cleanup_kproc();
        return -EIO;
    }
    root.misc_net_registered = TRUE;
    
    // Load threads for all the online CPUs
    for (n = 0; n < num_online_cpus(); n++) {
        t = (tok_thread_t*)per_cpu_ptr(root.thread_percpu, n);
    
        memset(t, 0, sizeof(tok_thread_t));
        init_completion(&t->rendez);

        t->task = kthread_create(kthread, t, "tokmodule(%d)", n);
        if (t->task == NULL || IS_ERR(t->task))
        {
            printk(KERN_CRIT "toknet: Can't create kernel thread for processing\n");
            cleanup_kproc();
            return -EIO;
        }
        
        t->cpu = n;
        kthread_bind(t->task, n);        
        wake_up_process(t->task);    
        wait_for_completion(&t->rendez);
        init_completion(&t->rendez);
    }
    
    // Add the packet receive hooks
    dev_add_pack(&tok_pt_net);
    dev_add_pack(&tok_pt_ip6);
    
    // Success the module is loaded!
    return 0;
};

module_init(init_kproc);
module_exit(cleanup_kproc);
        
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Tokera Virtual Storage Driver");
MODULE_AUTHOR("Johnathan Sharratt <support@tokera.com>");