/* 
 * File:   tokdefine.h
 * Author: root
 *
 * Created on 2 December 2014, 1:04 AM
 */

#ifndef TOKDEFINE_H
#define	TOKDEFINE_H

#ifdef	__cplusplus
extern "C" {
#endif

// #############################   
// Uncomment the below line to turn on debugging
#define TOK_DEBUG
//#define TOK_DEBUG_MORE

// #############################
#define TOK_ENABLE_ENCRYPTION
//#define TOK_ENABLE_QUEUING
#define TOK_ENABLE_THROTTLING
//#define TOK_ENABLE_FORCE_REBUILD
#define TOK_ENABLE_REPLICATION
//#define TOK_ENABLE_SKBUFF_FLAGS
//#define TOK_ENABLE_SAMPLE_ON_OPEN
//#define TOK_ENABLE_WORKER_PAGE_BALANCING
//#define TOK_ENABLE_FREE_SPACE_SCANNING
//#define TOK_ENABLE_MIRROR_BALANCING
#define TOK_ENABLE_ASYNC_HEAD
#define TOK_ENABLE_TIDY_ON_CLOSE
//#define TOK_ENABLE_READAHEAD
#define TOK_ENABLE_RTT_SAMPLING
#define TOK_ENABLE_SYNC_IO
#define TOK_ENABLE_READ_BALANCING
// #############################

// #############################    
#ifdef TOK_DEBUG
#define TOK_DEBUG_PROFILE
//#define TOK_DEBUG_DEEP_PROFILE
//#define TOK_DEBUG_SELF_TEST
//#define TOK_DEBUG_READAHEAD
//#define TOK_DEBUG_NET
//#define TOK_DEBUG_DEEP
//#define TOK_DEBUG_REF
//#define TOK_DEBUG_META
//#define TOK_DEBUG_EXTENT
//#define TOK_DEBUG_LOOKUP
//#define TOK_DEBUG_LOCK
//#define TOK_DEBUG_STACK
//#define TOK_DEBUG_PAGE
//#define TOK_DEBUG_BIO
//#define TOK_DEBUG_WAIT
//#define TOK_DEBUG_RANGE
//#define TOK_DEBUG_THROTTLE
//#define TOK_DEBUG_CONGESTION
//#define TOK_DEBUG_TIME
//#define TOK_DEBUG_FAIL_STACK
//#define TOK_DEBUG_DATA
//#define TOK_DEBUG_CMPXCHG_RESTART
// #############################
    
#define DBG  printk
#define DBG1 printk
#else
#define DBG( a... )
#define DBG1( a... )
#endif

#define TOKFS_SUPER_MAGIC 0x746F6B66

#define tok_list_page_last(head) (list_entry((head)->prev, struct page, lru))
#define tok_list_page_first(head) (list_entry((head)->next, struct page, lru))

#define TOK_DPUT(dentry) if (dentry != NULL) { dput(dentry); dentry = NULL; }
#define TOK_DFREE(dentry) if (dentry != NULL) { d_drop(dentry); dput(dentry); dentry = NULL; }
#define TOK_CPUT(conn) { if (conn != NULL) { tok_conn_put(conn); conn = NULL; } }

// Other defines
#define TOK_DEVICE_PREFIX              "tokd"

#define TOK_ENCRYPT_KEY_SIZE           16                   // Encryption key length in bytes

#define TOK_CLOSE_SLEEP                400                  // Amount of time to wait for a connection to properly close
#define TOK_CLOSE_TIMEOUT              20000                // Amount of time to wait while closing a connection

#define TOK_MAX_SG                     (PAGE_SIZE / sizeof(struct scatterlist))
#define TOK_MAX_SPLICE_SG              4                    // Maximum number of splice SG's

#define TOK_IO_PENDING_MIN             16                   // Minimum number of IO operations that can be pending
#define TOK_IO_PENDING_MAX             256                  // Maxmimum number of IO operations that can be pending
#define TOK_INLINE_BIO_VECS            32                   // Maximum number of inline bio vecs
#define TOK_IO_MAX_RANGES              8                    // Maximum number of ranges before we resort to using memory allocation

#define TOK_UNLOCK_FLUSH_TIMEOUT       20000                // Amount of time to wait while flushing data after a disk is unlocked)
#define TOK_SEM_TIMEOUT                5000                 // Amount of time before a lock on a file or volume times out (value must be less than TOK_MAX_PENDING_TIMEOUT)
#define TOK_LOCK_TIMEOUT               40000                // Amount of time before a lock on a file or volume times out (value must be less than TOK_MAX_PENDING_TIMEOUT)
#define TOK_LOCK_RELOCK                10000                // Amount of time between relocking an existing lock (value must be less than TOK_LOCK_TIMEOUT)
 
#define TOK_ROOT_TIMEOUT               4000                 // Timeout on commands that relate to the root
    
#define ETH_P_TOKNET                   0x0779

#define TOK_BD_FMODE                   FMODE_READ | FMODE_WRITE | FMODE_NOCMTIME           // O_RDWR | O_NONBLOCK | O_NOATIME    
    
#define TOK_FREE(a) { if (a != NULL) { kfree(a); a = NULL; } }

/* Ioctl defines */
#define TOKDISKSETCONFIG (('T'<< 8) | 200) 
#define TOKDISKGETCONFIG (('T'<< 8) | 201) 
#define TOKDISKINIT      (('T'<< 8) | 202) 
#define TOKRXRING        (('T'<< 8) | 203)
#define TOKTXRING        (('T'<< 8) | 204)
#define TOKNICDISK       (('T'<< 8) | 205)
#define TOKWAKE          (('T'<< 8) | 206)
#define TOKNETCONFIG     (('T'<< 8) | 207)
    
/* NIC ioctl defines */
#define TOKNETSETHANDLE  (SIOCDEVPRIVATE)

#ifndef KERNEL_SECTOR_SIZE
#define KERNEL_SECTOR_SIZE 512
#endif

#ifndef uint64_t
#define uint64_t long long unsigned int
#endif

#ifndef int32_t
#define int32_t int
#endif

#ifndef int16_t
#define int16_t short
#endif

#ifndef uint16_t
#define uint16_t unsigned short
#endif
    
#ifndef uint32_t
#define uint32_t unsigned int
#endif

#ifndef uint8_t
#define uint8_t unsigned char
#endif

#ifndef int8_t
#define int8_t char
#endif    
    
#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef ulong
#define ulong long unsigned
#endif

#ifndef MIN
#define MIN(a,b) (((a)<(b))?(a):(b))
#endif

#ifndef MAX
#define MAX(a,b) (((a)>(b))?(a):(b))
#endif

#ifndef ABS
#define ABS(a) (((a)>(0))?(a):(-a))
#endif
    
#ifndef INT64_MAX
# define INT64_MAX		(__INT64_C(9223372036854775807))
#endif
    
#ifndef UINT64_MAX
# define UINT64_MAX		(__UINT64_C(18446744073709551615))
#endif

#ifndef AES_BLOCK_SIZE
#define AES_BLOCK_SIZE 16
#endif
    
#ifndef IPPROTO_ETHERIP
#define IPPROTO_ETHERIP 97
#endif
    
#ifndef IPPROTO_IPV6TUNNEL
#define IPPROTO_IPV6TUNNEL 41
#endif
    
#ifndef IPPROTO_ESP
#define IPPROTO_ESP 50
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* TOKDEFINE_H */

