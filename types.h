/* 
 * File:   tokdisk.h
 * Author: Johnathan Sharratt
 * Created on 22 March 2014, 2:31 PM
 */
#ifndef TOKTYPES_H
#define TOKTYPES_H

enum tok_net_encrypt_mode {
    tok_net_encrypt_mode_none = 0,      // No encryption or decryption will be applied
    tok_net_encrypt_mode_encrypt = 1,   // The original packet will be encrypted before it is encapsulated
    tok_net_encrypt_mode_decrypt = 2,   // The original packet will be decrypted before it is decapsulated
    tok_net_encrypt_mode_never = 3      // Flag that indicates that encryption is disabled
};
typedef enum tok_net_encrypt_mode tok_net_encrypt_mode_t;

enum tok_net_encrypt_cipher {
    tok_net_encrypt_cipher_unknown = 0, // The cipher is not known
    tok_net_encrypt_cipher_aes128 = 1,  // Use the AES128 cipher
    tok_net_encrypt_cipher_aes256 = 2,  // Use the AES256 cipher
};
typedef enum tok_net_encrypt_cipher tok_net_encrypt_cipher_t;

enum tok_net_wrap_mode {
    tok_net_wrap_mode_none = 0,             // The packet will not be encapsulated
    tok_net_wrap_mode_encapsulate_ipv6 = 1, // The packet will be encapsulated in a Ipv6 packet
    tok_net_wrap_mode_decapsulate_ipv6 = 2,  // The packet will be decapsulated from a a Ipv6 packet
    tok_net_wrap_mode_encapsulate_ether = 3, // The packet will be encapsulated in a Ethernet packet
    tok_net_wrap_mode_decapsulate_ether = 4  // The packet will be decapsulated from a a Ethernet packet
};
typedef enum tok_net_wrap_mode tok_net_wrap_mode_t;

// Flags used by Tokera
enum {
    TOK_SKB_FLAG_PROCESSED = 1,     // The packet is (or should be) processed normally by Tokera
    TOK_SKB_FLAG_REPLY = 2,         // The packet is a reply to another packet and thus it should be steering to the right CPU
    TOK_SKB_FLAG_SPREAD = 4,        // Packet processing will be spread across the cores of the machine
};

//typedef unsigned long tok_time_t;
typedef int64_t tok_time_t;

struct pcpu_dstats {
    u64 tx_packets;
    u64 tx_dropped;
    u64 tx_bytes;

    u64 rx_packets;
    u64 rx_dropped;
    u64 rx_bytes;

    struct u64_stats_sync syncp;
};

/* Copyright (c) 2013 Coraid, Inc.  See COPYING for GPL terms. */
#define VERSION "85"
#define AOE_MAJOR 152
#define DEVICE_NAME "aoe"

/* set AOE_PARTITIONS to 1 to use whole-disks only
 * default is 16, which is 15 partitions plus the whole disk
 */
#ifndef AOE_PARTITIONS
#define AOE_PARTITIONS (16)
#endif

#define WHITESPACE " \t\v\f\n,"

#pragma pack(push)
#pragma pack(1)

struct tok_mac {

    union {

        struct {
            uint32_t high;
            uint16_t low;
        } rep1;

        struct {
#if defined(__LITTLE_ENDIAN_BITFIELD)
            uint8_t m1;
            uint8_t m2;
            uint8_t m3;
            uint8_t m4;
            uint8_t m5;
            uint8_t m6;
#else
            uint8_t m4;
            uint8_t m3;
            uint8_t m2;
            uint8_t m1;
            uint8_t m6;
            uint8_t m5;
#endif
        } rep2;

        struct {
            uint8_t m[6];
        } rep3;
    };
} typedef tok_mac_t;

struct tok_ip4 {
    union {
        struct
        {
            uint8_t             u1;
            uint8_t             u2;
            uint8_t             u3;
            uint8_t             u4;
            
            /*
#if BYTE_ORDER == LITTLE_ENDIAN
            uint8_t             u4;
            uint8_t             u3;
            uint8_t             u2;
            uint8_t             u1;
#else
            uint8_t             u1;
            uint8_t             u2;
            uint8_t             u3;
            uint8_t             u4;
#endif
            */
        } oct;
        uint8_t                 u8[4];
        uint16_t                u16[2];
        uint32_t                u32;
    };
    
} typedef tok_ip4_t;

struct tok_ip6 {
    union {
        uint8_t                 u8[16];
        uint16_t                u16[8];
        uint32_t                u32[4];
        uint64_t                u64[2];
    };
    
} typedef tok_ip6_t;

union tok_aes_key {
    uint8_t u8[TOK_ENCRYPT_KEY_SIZE];
    uint16_t u16[TOK_ENCRYPT_KEY_SIZE / 2];
    uint32_t u32[TOK_ENCRYPT_KEY_SIZE / 4];
} typedef tok_aes_key_t;

#pragma pack(pop)

struct tok_buf {
    char* data;
    int size;
    int maxsize;
} typedef tok_buf_t;

struct tok_id {

    union {
        uint8_t u8[16];
        uint16_t u16[8];
        uint32_t u32[4];
        uint64_t u64[2];
        uuid_t uuid;
    };
} typedef tok_id_t;

union etheriphdr {
    struct
    {
        uint16_t    version:4,
                    reserved:12;
    };
    uint16_t        encoded;
} __attribute__((packed)) typedef etheriphdr_t;

struct toknethdr {
    union
    {
        struct
        {
            // Note the first 32 bytes are aligned so that they represent unique data streams
            // While the second is used for further fields and for a sequence number
            
            uint64_t    flow_id:8,          // Flow ID used to direct traffic to a particular stream
                        affinity:8,         // CPU Affinity of the source (or destination) depending on direction
                        segment:36,         // Virtual segment that the packet relates too
                        flow_seq:8,         // Sequence ID attached to the flow (used for encrypt IV generation)
                        mode:3,             // Direction of the packet (is it a request or a response?)
                        encrypted:1;        // Flag that indicates if the packet has been encrypted or not
                        
        } u;
        struct
        {
            uint32_t    spi;
            uint32_t    seq;
        } hdr;
    };
    tok_mac_t           br_src;            // Source ID of the bridge that transmitted this packet
    tok_mac_t           br_dst;            // Destination ID of the bridge that transmitted this packet
} __attribute__((packed)) typedef toknethdr_t;

/*
 * Represents a virtual networking header used on all encapsulated packets
 */
struct vnhdr {
    
    tok_id_t segment_id;    // ID of the segment this virtual networking relates to
    char padding[2];        // Padding used to align the next Ethernet header on the 8 byte boundary
    
} __attribute__((packed)) typedef vnhdr_t;

enum {
	AOECMD_ATA,
	AOECMD_CFG,
	AOECMD_VEND_MIN = 0xf0,

	AOEFL_RSP = (1<<3),
	AOEFL_ERR = (1<<2),

	AOEAFL_EXT = (1<<6),
	AOEAFL_DEV = (1<<4),
	AOEAFL_ASYNC = (1<<1),
	AOEAFL_WRITE = (1<<0),

	AOECCMD_READ = 0,
	AOECCMD_TEST,
	AOECCMD_PTEST,
	AOECCMD_SET,
	AOECCMD_FSET,

	AOE_HVER = 0x10,
};

enum
{
	AOEERR_CMD= 1,
	AOEERR_ARG,
	AOEERR_DEV,
	AOEERR_CFG,
	AOEERR_VER,
};

struct aoe_short_hdr {
    unsigned char verfl;
    unsigned char err;
    __be16 major;
    unsigned char minor;
    unsigned char cmd;
    __be32 tag;
    unsigned char data[0];
} __attribute__((packed)) typedef aoe_short_hdr_t;

struct aoe_hdr {
    union
    {
        struct ethhdr eth;
        struct 
        {
            unsigned char dst[6];
            unsigned char src[6];
            __be16 type;
            unsigned char verfl;
            unsigned char err;
            __be16 major;
            unsigned char minor;
            unsigned char cmd;
            __be32 tag;
            unsigned char data[0];
        };
    };
} __attribute__((packed)) typedef aoe_hdr_t;

struct aoe_atahdr {
	unsigned char aflags;
	unsigned char errfeat;
	unsigned char scnt;
	unsigned char cmdstat;
        union
        {
            struct
            {
                unsigned char lba0;
                unsigned char lba1;
                unsigned char lba2;
                unsigned char lba3;
                unsigned char lba4;
                unsigned char lba5;
            };
            unsigned char lba[6];
        };
	unsigned char res[2];
        unsigned char data[0];
} __attribute__((packed)) typedef aoe_atahdr_t;

#define TOK_NET_CONFIG_VERSION      114

/*
 * Represents a pipe to another device which can include encapsulation and encryption
 */
struct tok_net_config_pipe {
    char                    devname[IFNAMSIZ];      // Name of the other device that this pipe will send its packets to
    
    uint16_t                weight;                 // Weight applied to this pipe (zero equals not a member of the round robin)
    tok_mac_t               dest_mac;               // MAC address for the network device
    tok_mac_t               src_mac;                // MAC address for the network device

    tok_ip6_t               dest_ip;                // Where the carrier packet will be sent to    
    tok_ip6_t               src_ip;                 // Where the carrier packet will be sent from
} __attribute__((packed)) typedef tok_net_config_pipe_t;

/* Represents a relay from one node to another */
struct tok_net_config_relay {
    tok_id_t                    node_from;
    tok_id_t                    node_to;
    
    tok_mac_t                   mac_from;
    tok_mac_t                   mac_to;
    
    tok_ip6_t                   ip_from;
    tok_ip6_t                   ip_to;
    
    int                         depth;
    int                         mtu;
    
    tok_mac_t                   br_dst;
    
    char                        device[IFNAMSIZ];
    int                         speed;    
} __attribute__((packed)) typedef tok_net_config_relay_t;

/*
 * Represents the configuration for the virtual network port
 */
struct tok_net_config {
    int                     version;                // Version of this configuration block
    
    int                     sizeof_net;
    int                     sizeof_pipe;
    int                     sizeof_relay;
    
    // [16 Bytes]
    tok_id_t                id;                     // ID of the virtual segment
    
    char                    brname[IFNAMSIZ];       // Name of the bridge this device will be attached to
    char                    devname[IFNAMSIZ];      // Name of the device that this configuration relates to
    int                     mtu;                    // MTU assigned to this particular device
    
    uint32_t                encrypt_mode:8,         // Mode that the encryption phase will run in
                            reserved_flags:22,      // Bunch of reserved flags
                            global_encryption:1,    // Flag that indicates if global encryption is enabled or not
                            local_encryption:1;     // Flag that indicates if local encryption is enabled or not
    int                     encrypt_cipher;         // ID of the cipher to use for encryption
    int                     wrap_mode;              // Indicates if the network packet will be encapsulated or not
    tok_aes_key_t           encrypt_key;            // The encryption key (if applicable) used to encrypt the packet body
    
    tok_mac_t               gateway_mac;            // MAC address of the gateway to the outside world
    tok_mac_t               br_src;                 // Source address of the bridge
    tok_mac_t               br_dst;                 // Destination address of the bridge

    int                     num_pipes;
    int                     num_relays;
    
} __attribute__((packed)) typedef tok_net_config_t;

/*
 * Represents all the flags currently set on the network device
 */
union tok_net_flags
{
    struct
    {
        // [4 Bytes]
        uint8_t             encrypt_mode;           // Mode that the encryption phase will run in
        uint8_t             encrypt_cipher;         // ID of the cipher to use for encryption
        uint8_t             wrap_mode;              // Indicates if the network packet will be encapsulated or not    
        uint8_t             reserved1:6,
                            global_encryption:1,    // Flag that indicates if global encryption is enabled or not
                            local_encryption:1;     // Flag that indicates if local encryption is enabled or not
    };
    uint32_t                all;                    // All of the flags combined are saved in this one structure
} __attribute__((packed)) typedef tok_net_flags_t;

/*
 * Represents an extent allocated to a particular volume
 */
struct tok_net {
    
    // The most important fields are packed into the first cache line
    // [16 Bytes]
    struct list_head        pipes;                  // List of all the pipes that this network device currently has allocated
    struct list_head        relays;                 // List of all the relays that this network device currently has allocated
    
    // [16 Bytes]
    struct list_head        head;                   // Head of the linked list that this network device is attached to along with all other devices
    
    // [8 Bytes]
    struct net_device*      dev;                    // Reference to the network device used for communication
    tok_net_flags_t         flags;                  // All the flags currently set for this network device
    
    // [16 Bytes]
    tok_mac_t               br_src;                 // Source address of the bridge
    tok_mac_t               br_dst;                 // Destination address of the bridge
    uint32_t                lb_iv;                  // Initialization vector for the load balancing algorithm for better entropy
    
    // [16 Bytes]
    union
    {
        tok_id_t            id;                     // ID of the virtual segment
        uint64_t            seg:36,                 // ID used to send down the ESP packets
                            reserved:28;            // Area reserved for the encryption flag
    };
    
    // [8 Bytes]
    tok_mac_t               gateway_mac;            // MAC address of the gateway device
    uint16_t                padding;                // Padding to align with 32bits

    // [8 Bytes]
    int                     total_weight;           // Total weight of all the pipes (used when calculating the round robin)
    
    // [8 Bytes]
    struct crypto_blkcipher *   encrypt_tfm;        // Reference to the block cipher used for encryption

    // [16 Bytes]
    tok_aes_key_t           encrypt_key;            // The encryption key (if applicable) used to encrypt the packet body
            
    // [8 Bytes]
    struct net_device*      br_dev;                 // Device that the bridge uses
    
    // [16 Bytes]
    char                    devname[IFNAMSIZ];      // Name of the device that this configuration relates to
    
    struct net*             ns;                     // Reference to the namespace for this device
        
} ____cacheline_aligned_in_smp typedef tok_net_t;

/*
 * Represents a pipe to another device
 */
struct tok_net_pipe {

    // The most important fields are packed into the first cache line
    // [16 Bytes]
    struct list_head        head;                   // Head of the linked list that that forms a list of the pipes a device has configured

    // [8 Bytes]
    struct net_device*      dev;                    // Pointer to the network device that this pipe is connected too (if any)
    
    // [8 Bytes]
    uint16_t                weight;                 // Weight applied to this pipe (zero equals not a member of the round robin)
    tok_mac_t               dest_mac;               // MAC address to transmit packets on
            
    // [32 Bytes]
    tok_ip6_t               dest_ip;                // IPv6 to transit packets to during encapsulation
    tok_ip6_t               src_ip;                 // IPv6 to transit packets from during encapsulation
    
    // [6 Bytes]
    tok_mac_t               src_mac;                // MAC address to receive packets on
    
    // [16 Bytes]
    char                    devname[IFNAMSIZ];      // Name of the other device that this pipe will send its packets to

    // [8 Bytes]
    struct list_head        del;                    // Head used for cleaning up old pipes
    
} ____cacheline_aligned_in_smp typedef tok_net_pipe_t;

struct tok_balance {
    tok_net_pipe_t*         pipe;                   // Reference to the pipe that the data will be transmitted down
    int                     parallel;               // Indicates that the packets can run in parallel
    int                     flow_id;                // ID used to steer packets into particular flows
} typedef tok_balance_t;

struct tok_thread {

    struct sk_buff_head skb_inq;
    struct sk_buff_head skb_outq;
    
    struct completion   rendez;
    struct task_struct* task;
    
    int                 cpu;
    int                 seq;
    
} ____cacheline_aligned_in_smp typedef tok_thread_t;

/*
 * Represents all the structures required to manage IO operations on the block device
 */
struct tokroot {

    struct kmem_cache*  net_cache; // Cache for extent buffers
    struct kmem_cache*  net_pipe_cache; // Cache for net relays
    
    struct list_head    net_list;

    struct page*        zero_page; // Reference to a zero page used by the driver
    
    spinlock_t          sys_lock;
    
    tok_thread_t*       thread_percpu;  // Thread dedicated to each CPU
    
    int misc_net_registered; // Flag that indicates if the misc device has been registered

} typedef tokroot_t;

#endif /* TOKTYPES_H */

