CC=gcc
KERNELDIR := ./kernel.d/headers.d/build
KCFLAGS = -c -g -Wall -I$(KERNELDIR)/include -D__KERNEL__ -DMODULE

M_OBJS = main.o chr.o net.o helper.o
M_TARGET = tokmodule.ko

obj-m := tokmodule.o
tokmodule-objs := $(M_OBJS)

all:
	./install-dep.sh
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules

clean:
	rm -f *.o *.ko *.mod.c

deepclean: clean
	-rm headers.zip
	-rm -r -f kernel.d/headers.d
