#include "main.h"

/*
 * Creates a new virtual network device that can be manipulated via the
 * character device returned by this function
 */
int tok_chr_net_open(struct inode *inode, struct file * file)
{
    // Declare variables    
    printk(KERN_INFO "toknet: Opening TokNet interface\n");
    
    // Validate
    if (unlikely(file == NULL)) {
        printk(KERN_ERR "toknet: Attempted to open a new TokNet interface with an invalid file pointer\n");
        return -EBADFD;
    }
    
    // Clear the private area
    file->private_data = NULL;
    
    DBG1(KERN_INFO "toknet: tok_chr_net_open\n");
    return 0;
};

/*
 * Closes a virtual network device and frees all the resources that
 * it had been consuming
 */
int tok_chr_net_close(struct inode *inode, struct file *file)
{    
    // Declare variables
    tok_net_t* net = NULL;
    
    DBG(KERN_INFO "toknet: tok_chr_net_close\n");
    
    // If it is a corrupted state then we can't release the resources
    net = (tok_net_t*)file->private_data;
    if (unlikely(net == NULL))
        return 0;
    file->private_data = NULL;
    
    // Release our reference to the device and all its resources
    // (while under a system up so that objects can be safely deleted)
    spin_lock(&root.sys_lock);
    printk(KERN_INFO "toknet: Destroying a closed TokNet connection\n");
    list_del_rcu(&net->head);
    spin_unlock(&root.sys_lock);
    
    // Break all the pipe references
    tok_net_refresh_pointers();
    
    // Get all the readers off the device
    synchronize_rcu();
    
    // Now release it
    tok_net_release(net);
    
    // Success
    return 0;
};

/*
 * Performs a configuration operation on the virtual network device
 */
long tok_chr_net_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    // Declare variables
    tok_net_t* net;
    tok_net_pipe_t* pipe;
    tok_net_pipe_t* npipe;
    
    // Create a list that will hold pipes that are to be destroyed
    struct list_head del_pipes;
    INIT_LIST_HEAD(&del_pipes);
    
    // Enter a system lock that allows for configuration changes
    spin_lock(&root.sys_lock);

    // Perform an action based on the command
    switch (cmd)
    {
        case TOKNETCONFIG:
        {
            struct net_device*  dev;
            tok_net_config_t    pre_config;
            tok_net_config_t*   config;
            tok_net_config_pipe_t*  config_pipes;
            tok_net_config_relay_t* config_relays;
            int                 config_size;
            int                 n;
            int                 err;
            int                 total_weight;
            
#ifdef TOK_DEBUG_MORE
            DBG(KERN_INFO "toknet: tok_chr_net_ioctl - virtual port init\n");
#endif
            if (copy_from_user(&pre_config, (const void*)arg, sizeof(tok_net_config_t)) != 0)
            {
                printk(KERN_ERR "toknet: Failed to read network config from user space\n");
                spin_unlock(&root.sys_lock);
                return -EFAULT;
            }
            
            // If the configuration version does not match then fail
            if (pre_config.version != TOK_NET_CONFIG_VERSION ||
                pre_config.sizeof_net != sizeof(tok_net_config_t) ||
                pre_config.sizeof_pipe != sizeof(tok_net_config_pipe_t) ||
                pre_config.sizeof_relay != sizeof(tok_net_config_relay_t)) {
                printk(KERN_ERR "toknet: The network interface configuration uses an unsupported version (version=%d, expected=%d, sizeof(tok_net_config_t)[%d vs %d], sizeof(tok_net_config_pipe_t)[%d vs %d], sizeof(tok_net_config_relay_t)[%d vs %d])\n", pre_config.version, TOK_NET_CONFIG_VERSION, (int)pre_config.sizeof_net, (int)sizeof(tok_net_config_t), (int)pre_config.sizeof_pipe, (int)sizeof(tok_net_config_pipe_t), (int)pre_config.sizeof_relay, (int)sizeof(tok_net_config_relay_t));
                spin_unlock(&root.sys_lock);
                return -EFAULT;
            }
                        
            // Allocate the right sized buffer for the configuration
            config_size = sizeof(tok_net_config_t) + (pre_config.num_pipes * sizeof(tok_net_config_pipe_t)) + (pre_config.num_relays * sizeof(tok_net_config_relay_t));
            config = (tok_net_config_t*)kmalloc(config_size, GFP_KERNEL);
            if (config == NULL) {
                printk(KERN_ERR "toknet: Failed to allocate buffer to hold configuration\n");
                spin_unlock(&root.sys_lock);
                return -EFAULT;
            }
            config_pipes = (tok_net_config_pipe_t*)(((char*)config) + sizeof(tok_net_config_t));
            config_relays = (tok_net_config_relay_t*)(((char*)config) + sizeof(tok_net_config_t) + (pre_config.num_pipes * sizeof(tok_net_config_pipe_t)));
            
            if (copy_from_user(config, (const void*)arg, config_size) != 0)
            {
                printk(KERN_ERR "toknet: Failed to copy network config from user space\n");
                spin_unlock(&root.sys_lock);
                kfree(config);
                return -EFAULT;
            }
            
            // Search for an existing bridge with this particular ID
            net = tok_net_next(NULL);
            for (;net != NULL; net = tok_net_next(net)) {
                
                // If we have found it then exit the loop
                if (strncmp(net->devname, config->devname, IFNAMSIZ) == 0) {
#ifdef TOK_DEBUG_MORE
                    DBG(KERN_INFO "toknet: Updating the network interface (name=%s)\n", net->devname);
#endif
                    break;
                }
            }
            
            // If the private data needs to be released
            if (file->private_data != NULL && file->private_data != net) {
                printk(KERN_ERR "toknet: File pointer is attached to another device\n");
                spin_unlock(&root.sys_lock);
                kfree(config);
                return -EFAULT;
            }
            
            // Otherwise we may to create a new one
            if (net == NULL) {
#ifdef TOK_DEBUG
                DBG(KERN_INFO "toknet: Creating the network interface (name=%s num_pipes=%d mac=%pM)\n", config->devname, config->num_pipes, &config->br_dst);
#endif

                // Enter a network lock
                rtnl_lock();
                
                // Attempt to create the device
#if LINUX_VERSION_CODE > KERNEL_VERSION(3,17,0)
                dev = alloc_netdev(sizeof(tok_net_t), config->devname, NET_NAME_UNKNOWN, tok_net_setup);
#else
                dev = alloc_netdev(sizeof(tok_net_t), config->devname, tok_net_setup);
#endif
                if (dev == NULL) {
                    DBG(KERN_ERR "toknet: Failed to create the network interface (name=%s)\n", config->devname);
                    rtnl_unlock();
                    spin_unlock(&root.sys_lock);
                    kfree(config);
                    return -EFAULT;
                }
#ifdef TOK_DEBUG
                DBG(KERN_INFO "toknet: Successfully created network interface (name=%s)\n", config->devname);
#endif
                
                // Set the private area
                net = (tok_net_t*)netdev_priv(dev);
                if (net == NULL) {
                    DBG(KERN_ERR "toknet: Failed to acquire the private region of the network device\n");
                    rtnl_unlock();
                    spin_unlock(&root.sys_lock);
                    kfree(config);
                    return -EFAULT;
                }
                memset(net, 0, sizeof(tok_net_t));
                INIT_LIST_HEAD(&net->pipes);;
                INIT_LIST_HEAD(&net->relays);
                
                // Set the basic properties
                get_random_bytes(&net->lb_iv, sizeof(uint32_t));
                net->dev = dev;
                memcpy(&net->br_src, &config->br_src, sizeof(tok_mac_t));
                memcpy(&net->br_dst, &config->br_dst, sizeof(tok_mac_t));
                
                /*
                struct net*         ns;
                struct net_device*  ns_dev;
                
                // Search for the namespace on the device we are intending to attach to
                for_each_net(ns) {
                    ns_dev = dev_get_by_name_rcu(ns, config->brname);
                    if (ns_dev != NULL)
                    {                
                        // We have found the namespace
                        net->ns = ns;
                        dev_net_set(dev, ns);
                        break;
                    }
                }
                */
                
                // Set the MAC address (if one was supplied)
                if (net->br_dst.rep1.high != 0 &&
                    net->br_dst.rep1.low != 0)
                {
                    dev->addr_assign_type = NET_ADDR_PERM;
                    memcpy(dev->dev_addr, &net->br_dst, ETH_ALEN);
                    memcpy(dev->perm_addr, &net->br_dst, ETH_ALEN);
                    dev->addr_len = ETH_ALEN;
                }
                
                // Register and set the network device
                dev->rtnl_link_ops = &tok_net_link_ops;
                err = register_netdevice(dev);                
                if (err < 0) {
                    DBG(KERN_ERR "toknet: Failed to create virtual network interface - could not register netdevice (err=%d)...continuing...\n", err);
                    rtnl_unlock();
                    spin_unlock(&root.sys_lock);
                    kfree(config);
                    free_netdev(dev);
                    return -EFAULT;
                }
                
                // Open the device
                err = dev_open(dev);
                if (err < 0) {
                    DBG(KERN_ERR "toknet: Failed to open virtual network interface - could not register netdevice (err=%d)...continuing...\n", err);
                    rtnl_unlock();
                    spin_unlock(&root.sys_lock);
                    kfree(config);
                    free_netdev(dev);
                    return -EFAULT;
                }
                
                // Leave the network lock
                rtnl_unlock();
                
                // Create the encryption object
                net->encrypt_tfm = crypto_alloc_blkcipher("ctr(aes)", 0, 0);
                if (net->encrypt_tfm == NULL) {                        
                    DBG(KERN_ERR "toknet: Failed to create the block cipher ctr(aes) for device [%s]\n", config->devname);
                    rtnl_unlock();
                    spin_unlock(&root.sys_lock);
                    kfree(config);
                    free_netdev(dev);
                    return -EFAULT;
                }
                
                // Add it to the tail of the network port list
                list_add_tail_rcu(&net->head, &root.net_list);
                
                // Debug output
                DBG(KERN_INFO "toknet: Net Created (from=%pM to=%pM)\n", &config->br_src, &config->br_dst);
            }
            file->private_data = net;
            
            // Update the MTU if its changed
            if (net->dev != NULL && config->mtu >= 68)
                net->dev->mtu = config->mtu;
                       
            // Update other certain properties
            memcpy(&net->id, &config->id, sizeof(tok_id_t));    // This will in effect set net->seg
            memcpy(net->devname, config->devname, IFNAMSIZ);
            memcpy(&net->gateway_mac, &config->gateway_mac, sizeof(tok_mac_t));
            
            memcpy(&net->br_src, &config->br_src, sizeof(tok_mac_t));
            memcpy(&net->br_dst, &config->br_dst, sizeof(tok_mac_t));
            
            if (net->flags.encrypt_mode != config->encrypt_mode) {
                net->flags.encrypt_mode = config->encrypt_mode;
                DBG(KERN_INFO "toknet: Net (encrypt-mode=%d)\n", config->encrypt_mode);
            }
            if (net->flags.encrypt_cipher != config->encrypt_cipher) {
                net->flags.encrypt_cipher = config->encrypt_cipher;
                DBG(KERN_INFO "toknet: Net (encrypt-cipher=%d)\n", config->encrypt_cipher);
            }
            if (net->flags.wrap_mode != config->wrap_mode) {
                net->flags.wrap_mode = config->wrap_mode;
                DBG(KERN_INFO "toknet: Net (wrap-mode=%d)\n", config->wrap_mode);
            }
            if (net->flags.global_encryption != config->global_encryption) {
                DBG(KERN_INFO "toknet: Net (global-encryption=%d, was=%d)\n", config->global_encryption, net->flags.global_encryption);
                net->flags.global_encryption = config->global_encryption;
            }
            if (net->flags.local_encryption != config->local_encryption) {
                DBG(KERN_INFO "toknet: Net (local-encryption=%d, was=%d)\n", config->local_encryption, net->flags.local_encryption);
                net->flags.local_encryption = config->local_encryption;
            }                
            if (memcmp(&net->encrypt_key, &config->encrypt_key, sizeof(tok_aes_key_t)) != 0) {
                memcpy(&net->encrypt_key, &config->encrypt_key, sizeof(tok_aes_key_t));
                crypto_blkcipher_setkey(net->encrypt_tfm, (const u8*)&net->encrypt_key, sizeof(tok_aes_key_t));
            }
                        
            // Remove any pipes that are not in the configuration that was uploaded
            pipe = tok_net_pipe_next(net, NULL);
            for (;pipe != NULL; pipe = tok_net_pipe_next(net, pipe))
            {
                // If the pipe is not present in the new configuration list then remove it
                int exists = 0;
                for (n = 0; n < config->num_pipes; n++) {
                    if (strncmp(pipe->devname, config_pipes[n].devname, IFNAMSIZ) == 0) {
                        exists = 1;
                        break;
                    }
                }
                
                // If it no longer exists then kill it and restart the search
                if (exists == 0) {
                    
                    list_del_rcu(&pipe->head);  // This is already under a system lock
                    list_add_tail(&pipe->del, &del_pipes);
                    pipe = NULL;
                    continue;
                }
            }
            
            // Now cycle through all the pipes in the new configuration list and create them if they dont exist otherwise update them
            total_weight = 0;
            for (n = 0; n < config->num_pipes; n++) {
                int has_changed = 0;
                
                // Search to see if it already exists
                pipe = tok_net_pipe_next(net, NULL);
                for (;pipe != NULL; pipe = tok_net_pipe_next(net, pipe)) {
                    if (strncmp(pipe->devname, config_pipes[n].devname, IFNAMSIZ) == 0) {
#ifdef TOK_DEBUG_MORE
                        DBG(KERN_INFO "toknet: Found existing pipe (from=%s to=%s)\n", config->devname, config_pipes[n].devname);
#endif
                        break;
                    }
                }
                
                // It does not so we need to create it
                if (pipe == NULL) {
                    pipe = tok_net_pipe_create();
                    
#ifdef TOK_DEBUG
                    DBG(KERN_INFO "toknet: Successfully created pipe (from=%s to=%s)\n", config->devname, config_pipes[n].devname);
#endif
                    
                    memcpy(pipe->devname, config_pipes[n].devname, IFNAMSIZ);
                
                    list_add_tail_rcu(&pipe->head, &net->pipes);
                    has_changed = 1;
                }
                
                // Set all the properties for this pipe
                if (pipe->weight != config_pipes[n].weight) {
                    pipe->weight = config_pipes[n].weight;
                    has_changed = 1;
                }
                if (memcmp(&pipe->dest_mac, &config_pipes[n].dest_mac, sizeof(tok_mac_t)) != 0) {
                    memcpy(&pipe->dest_mac, &config_pipes[n].dest_mac, sizeof(tok_mac_t));
                    has_changed = 1;
                }
                if (memcmp(&pipe->dest_ip, &config_pipes[n].dest_ip, sizeof(tok_ip6_t)) != 0) {
                    memcpy(&pipe->dest_ip, &config_pipes[n].dest_ip, sizeof(tok_ip6_t));
                    has_changed = 1;
                }
                if (memcmp(&pipe->src_mac, &config_pipes[n].src_mac, sizeof(tok_mac_t)) != 0) {
                    memcpy(&pipe->src_mac, &config_pipes[n].src_mac, sizeof(tok_mac_t));
                    has_changed = 1;
                }
                if (memcmp(&pipe->src_ip, &config_pipes[n].src_ip, sizeof(tok_ip6_t)) != 0) {
                    memcpy(&pipe->src_ip, &config_pipes[n].src_ip, sizeof(tok_ip6_t));
                    has_changed = 1;
                }
                
                // Increment the total weight
                total_weight += pipe->weight;
                
                // Show debug information only if the device actually changed
                if (has_changed == 1) {
                    DBG(KERN_INFO "toknet: Pipe Device (portname=%s, name=%s, weight=%d, wrap=%d)\n", config->devname, config_pipes[n].devname, config_pipes[n].weight, net->flags.wrap_mode);
                    DBG(KERN_INFO "toknet: Pipe MAC Addresses (from=%pM to=%pM)\n", &config_pipes[n].src_mac, &config_pipes[n].dest_mac);
                    DBG(KERN_INFO "toknet: Pipe IP6 Addresses (from=%pI6c to=%pI6c)\n", &config_pipes[n].src_ip, &config_pipes[n].dest_ip);
                    
                    // Output the pipe
                    if (net->flags.encrypt_mode == tok_net_encrypt_mode_encrypt || net->flags.encrypt_mode == tok_net_encrypt_mode_decrypt) {
                        //DBG(KERN_INFO "toknet: Pipe (encrypt-key=%16phN)\n", &net->encrypt_key);
                        DBG(KERN_INFO "toknet: Pipe (encrypt-mode=%d)\n", net->flags.encrypt_mode);
                    }
                }
            }
            
            // Set the total weight
            net->total_weight = total_weight;
            
            // Refresh all the network device pointers for all the pipes
            tok_net_refresh_pointers();
            
            // Release the configuration structure as we have now updated everything
            kfree(config);
            break;
        }
        default:
        {
            printk(KERN_WARNING "tokmodule (ioctl): Invalid ioctl command (%d) was passed to TokNet connection\n", cmd);
            spin_unlock(&root.sys_lock);
            return -EINVAL;
        }
    }
    
    // Release the lock
    spin_unlock(&root.sys_lock);
    
    // If we have pipes to clean up
    if (del_pipes.next != &del_pipes)
    {
        // Update the pointers
        tok_net_refresh_pointers();
        
        // Now get everybody off the deleted pipes
        synchronize_rcu();
        
        // Loop through all the pipes that need to be destroyed
        for (pipe = tok_net_pipe_next_list_del(&del_pipes, NULL); pipe != NULL; pipe = npipe) {
            npipe = tok_net_pipe_next_list_del(&del_pipes, pipe);
            
            // Release the pipe
            tok_net_pipe_release(pipe);
        }
    }
    
    return 0;
};

// All the file operation callback functions that are invoked when the
// device driver user is manipulating an instance of a virtual network device
struct file_operations tok_chr_net_fops =
{
    .owner              = THIS_MODULE,	
    .llseek             = no_llseek,
    //.poll               = tok_chr_net_poll,
    .unlocked_ioctl 	= tok_chr_net_ioctl,
    //.mmap               = tok_chr_net_mmap,
    .open               = tok_chr_net_open,
    .release            = tok_chr_net_close
};

// Configuration for the base character device that all the callers will interface
// with to create and manage virtual network devices
struct miscdevice tok_chr_net_dev =
{
    .minor = MISC_DYNAMIC_MINOR,
    .name = "toknet",
    .nodename = "toknet",
    .fops = &tok_chr_net_fops
};
