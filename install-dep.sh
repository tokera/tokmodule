#!/bin/sh -e

CURKV=$(uname -r)
PWD=$(pwd)
export PWD

echo "Installing package dependencies...."
if [ ! -f "/usr/bin/gcc" ]; then
  sudo apt-get -y install gcc
fi

if [ ! -f "/usr/bin/lintian" ]; then
  sudo apt-get -y install lintian
fi
if [ ! -d "/var/lib/dpkg" ]; then
  sudo apt-get -y install dpkg
fi
#apt-get -y install md5deep

# Download the kernel
echo "Downloading Tokera kernel (if not already present)..."
mkdir -p kernel.d
if [ -f "vmlinuz" ]; then
  if [ -f "kernel.d/vmlinuz" ]; then
    rm kernel.d/vmlinuz
  fi
  cp vmlinuz kernel.d
elif [ ! -f "kernel.d/vmlinuz" ]; then
  if [ -f "../tokkernel/binary.d/vmlinuz" ]; then
    cp ../tokkernel/binary.d/vmlinuz kernel.d/vmlinuz
  else
    cd kernel.d
    wget --no-check-certificate https://ci.tokera.com/guestAuth/repository/download/Tokera_Kerrnel_Build/.lastSuccessful/vmlinuz
    cd ..
  fi
fi
echo "Done"

echo "Downloading Tokera kernel headers..."
# Install the headers if they exist
HEADFILE=
[ -f "headers.zip" ] && HEADFILE="headers.zip"
[ -f "../tokkernel/headers.zip" ] && HEADFILE="../tokkernel/headers.zip"
[ -z "$HEADFILE" ] && echo "No kernel headers file" && exit 11

if [ -f "$HEADFILE" ]; then
  NEWMD5=$(md5sum $HEADFILE)
  OLDMD5=""
  if [ -f "headers.d/md5" ]; then
    OLDMD5=$(cat headers.d/md5)
  fi
  if [ -f "kernel.d/headers.d/md5" ]; then
    OLDMD5=$(cat kernel.d/headers.d/md5)
  fi
  if [ "$NEWMD5" != "$OLDMD5" ]; then
    if [ -d "headers.d" ]; then
      rm -r headers.d
    fi
    if [ ! -f headers.zip ]; then
      cp -f $HEADFILE .
      unzip headers.zip
      rm -f headers.zip
    else
      unzip headers.zip
    fi
    echo "$NEWMD5" > headers.d/md5
  fi
else
  echo "Error: No kernel headers file"
  exit -1
fi
if [ -d "headers.d" ]; then
  if [ -d "kernel.d/headers.d" ]; then
    rm -r kernel.d/headers.d
  fi
  mkdir -p kernel.d
  mv headers.d kernel.d
fi
echo "Done"

# Make sure the perf is compiled
cd kernel.d/headers.d/build/tools/perf
[ ! -f perf ] && make
cd $PWD
